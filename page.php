<?php get_header('default'); ?>
	<div class="row">

		<?php get_sidebar(); ?>

		<div class="span9">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="page-header">
					<h1>
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title() ?></a>
						<small><?php the_excerpt() ?></small>
					</h1>
					<?php the_breadcrumb(); ?>
				</div>

				<article><?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?></article>
			</div>
			<?php endwhile; else: ?>
			<?php endif; ?>
			<?php wp_link_pages(); ?>			
		</div>
    
	</div><!--.row-->
<?php get_footer('default'); ?>