<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

<?php

 $querystr = "
    SELECT $wpdb->posts.* 
    FROM $wpdb->posts, $wpdb->postmeta
    WHERE $wpdb->posts.ID = $wpdb->postmeta.post_id 
    AND $wpdb->postmeta.meta_key = 'tag' 
    AND $wpdb->postmeta.meta_value = 'email' 
    AND $wpdb->posts.post_status = 'publish' 
    AND $wpdb->posts.post_type = 'post'
    AND $wpdb->posts.post_date < NOW()
    ORDER BY $wpdb->posts.post_date DESC
 ";

 $pageposts = $wpdb->get_results($querystr, OBJECT);

global $campos_mb;
$loop = new WP_Query( array(
	'post_type' => 'anuncios',
	'posts_per_page' => 100,
	'meta_key' => 'kadum_campos',
	'area_atuacao' => $wp_query->query_vars['area_atuacao'],

));

if( $loop->have_posts() ){

	for($i=0; $loop->have_posts(); $i++){

		$loop->the_post();
		$meta = $campos_mb->the_meta();
		
		echo ( (($i % 3)==0) ) ? '<div class="row">' : '';

?>
		<div class="span3">

			<div class="thumbnail">
              <img alt="" src="http://lorempixel.com/270/135/">
              <div class="caption">
				<?php //echo $meta['plano']; ?>
                <h3><a href="<?php the_permalink() ?>"><?php echo $meta['titulo']; ?></a></h3>
                <p><?php echo $meta['descricao']; ?></p>
                <p><a class="btn" href="<?php the_permalink() ?>"><i class="icon-search"></i></a></p>
              </div>
            </div>

		</div>		
<?php
		echo ( ($i+1)==$loop->post_count || ((($i+1) % 3)==0) ) ? "</div><!-- .row -->" : '';
	};

?>
	<?php if( $wp_query->max_num_pages > 1 ): ?>
		<div id="nav-above" class="navigation">
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyten' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
		</div><!-- #nav-above -->
	<?php endif; ?>

<?php
}else{

	echo "Não há posts";

}; ?>