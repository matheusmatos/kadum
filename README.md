Tema personalizado e exclusivo para ser usado no Kadum.

Alguns recursos:

01. Codificado seguindo padrões e convenções para um código limpo, flexível e escalonável.
02. Página de Opções com diversas opções de personalização e configuração.
03. Tipos de posts personalizados para os anúncios
04. Taxonomias para as cidades e áreas de atuação
05. Campos personalizados para criar os anúncios
06. Validação ajax na criação do anúncio
07. Integração com o PagSeguro para o pagamento do anúncio
08. Grupo personalizado de usuários para os Anunciantes
09. Cadastro de usuários no Wordpress com a classe própria KadumCadastro
10. A classe KadumCadastro cadastra com GooglePlus, Facebook, Twitter, Email, e escalona para qualquer outra API
11. Personalização do Painel do Wordpress, modificando completamente a aparência de Login e do Painel administrativo
12. Tarefas agendadas para enviar emails quando anúncios estiverem próximos de expirar (CronJobs diários)
13. Widgets
14. Menus personalizados
15. Compilador PHP para Less
16. Frontend com TwitterBootstrap
17. Tema e código totalmente documentado e comentado em DOCX e PDF


Mais recursos e detalhes, na documentação, ou usando o Kadum!


Enjoy!