<?php get_header('default'); ?>
    
    <div class="row">
        <?php get_sidebar('subcat'); ?>

        <div class="span9">

            <div class="page-header center">
                <h1>Ofertas do Kadum</h1>
                <span>Lorem ipsum dolor sit amet</span>
                
                <?php the_breadcrumb(); ?>
            </div>

            <?php get_template_part('loop','area-atuacao'); ?>
        </div>
    
<?php get_footer('default'); ?>