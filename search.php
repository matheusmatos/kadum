<?php get_header('default'); ?>

<?php #get_sidebar(); ?>

<div class="content row">
	<div class="span12">
		
		<h2 class="page-header">Pesquisa por: <?php echo esc_attr(get_search_query()); ?></h1>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
			
				<h3 class="search-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			
				<p class="meta"><?php _e("Posted", "bonestheme"); ?> <time datetime="<?php echo the_time('Y-m-j'); ?>" pubdate><?php the_time('F jS, Y'); ?></time> <?php _e("by", "bonestheme"); ?> <?php the_author_posts_link(); ?> <span class="amp">&</span> <?php _e("filed under", "bonestheme"); ?> <?php the_category(', '); ?>.</p>
	
				<?php the_excerpt('<span class="read-more">Read more &raquo;</span>'); ?>
	
			</article> <!-- end article -->

			<!--
			<div id="post-<?php the_ID(); ?>" class="post-page">
				<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title() ?></a></h2>
				<div class="entry">
					<?php the_content('<p class="serif">Read the rest of this entry &raquo;</p>'); ?>
				</div>
			</div>
			-->
		<?php endwhile; ?>
		<?php else: ?>

		<?php endif; ?>
		<?php wp_link_pages(); ?>
	</div>


</div>
<?php get_footer('default'); ?>