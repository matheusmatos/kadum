<?php global $post ?>
<!doctype html>
<html dir="ltr" lang="pt-BR" class="no-js">
<head>
  
  <!-- Meta Tags -->
  <meta http-equiv="X-UA-Compatible" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="" />
  
  <title><?php wp_title("|", true, "right"); bloginfo("name"); ?></title>

  <!-- CSS -->
  <link rel="stylesheet" href="<?php echo assets_uri; ?>/css/bootstrap.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />  

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo assets_uri; ?>/img/ico/favicon.ico" type="image/x-icon">
  
  <!-- Ícones Mobile -->
  <link rel="apple-touch-startup-image" href="<?php echo assets_uri; ?>/img/ico/apple-startup.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo assets_uri; ?>/img/ico/apple-icon-57.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo assets_uri; ?>/img/ico/apple-icon-72.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo assets_uri; ?>/img/ico/apple-icon-114.png">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo assets_uri; ?>/img/ico/apple-icon-144.png">

  <!-- Imagens de inicialização nos gadgets Apple (startup/splash image) -->
  <link rel="apple-touch-startup-image" href="<?php echo assets_uri; ?>/img/ico/startup/apple-startup-320x460.png" />
  <link rel="apple-touch-startup-image" sizes="640x920" href="<?php echo assets_uri; ?>/img/ico/startup/apple-startup-640x920.png" />
  <link rel="apple-touch-startup-image" sizes="768x1004" href="<?php echo assets_uri; ?>/img/ico/startup/apple-startup-768x1004.png" />
  <link rel="apple-touch-startup-image" sizes="748x1024" href="<?php echo assets_uri; ?>/img/ico/startup/apple-startup-748x1024.png" />
  <link rel="apple-touch-startup-image" sizes="1496x2048" href="<?php echo assets_uri; ?>/img/ico/startup/apple-startup-1496x2048.png" />
  <link rel="apple-touch-startup-image" sizes="1536x2008" href="<?php echo assets_uri; ?>/img/ico/startup/apple-startup-1536x2008.png" />

  <!-- Info -->
  <meta name="description" content="<?php bloginfo('description'); ?>">
  <meta name="author" content="<?php bloginfo('name'); ?>">    
  <link rel="publisher" href="https://plus.google.com/100115661521059318646/" >
  <meta property="fb:admins" content="">
  <meta property="fb:app_id" content="">
  <meta property="fb:page_id" content="">
  <meta property="fb:image" content="">
  <meta property="og:site_name" content="<?php bloginfo('name'); ?>">
  <meta property="og:title" content="<?php bloginfo('name'); ?>" />
  <meta property="og:description" content="<?php bloginfo('description'); ?>">
  <meta property="og:locale" content="<?php bloginfo('Brasil'); ?>">
  <meta property="og:image" content="<?php echo assets_uri; ?>/img/logos/avatar.jpg">
  <meta property="og:url" content="<?php bloginfo('url'); ?>" />   
  <meta property="og:type" content="website">
      
  <!-- Outros -->
  <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <link rel="canonical" href="">
  <?php wp_head(); ?>

  <?php wp_get_archives("type=monthly&format=link"); ?>

</head>
<body <?php body_class(); ?>>

