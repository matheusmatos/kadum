<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Toda a personalização do Painel é iniciada aqui.
**/


# Adição da função no gancho 'after_setup_theme' do Wordpress
add_action('after_setup_theme', 'kadum_menu');

# Registro da taxonomia Área de Atuação para os anuncios
function kadum_menu() {

    # Adiciona Menus como um suporte do tema
    add_theme_support('menus');
    
    # Registra o menus disponíveis no tema
    register_nav_menus(
        array(
            'navegacao-superior'   => __('Navegação de páginas no cabeçalho'),   # Menu azul superior
            'navegacao-categorias' => __('Barra amarela de categorias'),         # Barra amarela
        )
    );        

};