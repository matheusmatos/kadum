<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * 
 * Continue a arte: código é poesia!
**/


/**
 *  Registro das taxonomias para os Anúncios
 *  
 *  Aqui são criadas as seguintes taxonomias:
 *  
 *  Áreas de Atuação;
 *  Cidades.
 *  
**/


# Adição da função no gancho 'init' do Wordpress
add_action('widgets_init', 'kadum_widgets_init');

function kadum_widgets_init() {
    
    /**
     * LATERAIS
     * O kadum possui alguns widgets que serão utilizados nas laterais do site.
     **/

    # LATERAL: Páginas
    register_sidebar(array(
        'name' => __('Lateral: Páginas', 'kadum-theme'),
        'id' => 'lateral-paginas-1',
        'description' => __('Você pode exibir o que desejar na lateral das páginas do Kadum.', 'kadum-theme' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    # LATERAL: Categorias
    register_sidebar(array(
        'name' => __('Lateral: Categorias', 'kadum-theme'),
        'id' => 'lateral-categorias-1',
        'description' => __('Você pode exibir o que desejar na lateral das páginas de categorias do Kadum.', 'kadum-theme' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));



    /**
     * RODAPÉS
     * O rodapé do Kadum é formado por três colunas, cada um, é um widget.
     **/

    # RODAPÉ: Coluna 1
    register_sidebar(array(
        'name' => __('Rodapé: Coluna 1', 'kadum-theme'),
        'id' => 'rodape-1',
        'description' => __('Você pode exibir o que desejar em uma das três colunas no rodapé do Kadum.', 'kadum-theme' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    # RODAPÉ: Coluna 2
    register_sidebar(array(
        'name' => __('Rodapé: Coluna 2', 'kadum-theme'),
        'id' => 'rodape-2',
        'description' => __('Você pode exibir o que desejar em uma das três colunas no rodapé do Kadum.', 'kadum-theme' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    # RODAPÉ: Coluna 3
    register_sidebar(array(
        'name' => __('Rodapé: Coluna 3', 'kadum-theme'),
        'id' => 'rodape-3',
        'description' => __('Você pode exibir o que desejar em uma das três colunas no rodapé do Kadum.', 'kadum-theme' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
}