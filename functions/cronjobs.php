<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * CronJobs, aqui são agendadas tarefas no Kadum.
**/


/*** GANCHOS -***/

add_action('evento_diario', 'anuncios_expirados');

add_action('evento_horario', 'avisar_anunciantes');

add_action('wp', 'registrar_cronjobs');

function registrar_cronjobs() {
    if ( !wp_next_scheduled('evento_diario') ){
        wp_schedule_event( time(), 'daily', 'evento_diario');
    }

    if ( !wp_next_scheduled('evento_horario') ){
        wp_schedule_event( time(), 'daily', 'evento_horario');
    }    
}


/**
 * Esta tarefa pega anúncios que vencem hoje e marca-os como expirados.
**/
function anuncios_expirados() {

    // procura por anúncios que já expiraram

    $query = new WP_Query(array(
        'post_status' => 'expirado',
        'post_type' => 'anuncios',
    ));
    
    // The Loop
    while( $the_query->have_posts() ) :

        $plano   = get_post_meta($post->ID, 'plano', true);
        $prazo   = $opcoes['plano'.$plano.'_periodo'];
        $expira  = date('Y-m-d', strtotime($post->post_date . ' +'.$prazo.'days') );
        $hoje    = date('Y-m-d');
        $nome    = date('d/m/Y', strtotime($post->post_date . ' +'.$prazo.'days') );

        if( $post->post_status != 'expirado' && (strtotime($hoje) > strtotime($expira)) ):
            $post->post_status = 'expirado';
            wp_update_post($post);
        endif;

    endwhile;

}


/**
 * Esta tarefa procura por anuncios que estão próximos de expirar, e avisa
 * aos respectivos anunciantes, para que possam renová-lo dentro do tempo.
**/
function avisar_anunciantes() {

    // procura anúncios próximos de expirar
    // WP_Query meta_value e tal.

    // percorre eles, pega os emails de cada um,
    // e envia um email para todos.

    // especificar um limite
    // caso não seja possível fazer em uma hora, na próxima será.

    // WP_Query para obter os posts (meta_key='expira', meta_value= maior que hoje + Xdias )
    // Entra no loop..
    // get_author pelo ID do post
    // wp_mail para o email do autor
    // update_post_meta -->> meta_key = 'avisado', meta_value = true
}