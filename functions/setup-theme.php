<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 *
 * Aqui é feito o registro das seguintes taxonomias para os Anúncios: Áreas de Atuação e Cidades.
 * Aqui também são adicionadas automaticamente as Áreas de Atuação aos Menus do Wordpress.
 *
**/


# Executa a função após a instalação do tema. Consultar o gancho 'after_setup_theme' do Wordpress
add_action('after_setup_theme', 'kadum_setup_theme');

# Adiciona as opções no banco de dados quando o tema é instalado
function kadum_setup_theme() {
  
  global $pagenow;

  if( $pagenow == 'themes.php' && (isset($_GET['activated'])) ) {

    # Array para reescrever configurações padrões do Wordpress
    $core_opcoes = array(
      'avatar_default' => 'mystery',
      'default_role'   => 'anunciante',
    );

    # Reescreve configurações padrões do Wordpress
    foreach($core_opcoes as $k => $v ) update_option( $k, $v );

    $msg = '
    <div class="updated">
      <p>O tema "' . get_option( 'current_theme' ) . '" modificou algumas das suas <a href="' . admin_url( 'options-general.php' ) . '" title="Ir para as configurações.">configurações</a> padrões do WordPress.</p>
    </div>
    <div class="updated">
      <p>Você pode <a href="' . admin_url('admin.php?page=opcoes-kadum') . '" title="Ir para as configurações do tema.">configurar</a> o tema "' . get_option( 'current_theme' ) . '".</p>
    </div>';
    add_action('admin_notices', $c = create_function('', 'echo "' . addcslashes( $msg, '"' ) . '";' ) );
  
  }

}