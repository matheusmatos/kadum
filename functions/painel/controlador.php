﻿<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Toda a personalização do Painel é iniciada aqui.
**/





/*** GANCHOS ***/

/*
 * Para desativar a estilização do painel basta
 * comentar/remover estes dois primeiros ganchos.
 * ------> Mas não faça isso!!! :(
 * ------> Foi feito especialmente para o Kadum. :)
**/


# ------> Se quiser ativar apenas para os anunciantes,
# ------> seria algo absurdamente simples! Veja:

# Obtendo o grupo ao qual pertence o usuário atual
global $current_user; get_currentuserinfo();
$role_current_user = array_shift($current_user->roles);

//if( true ){ // pode remover o IF e as 2 linhas acima se for deixar para todos

if( isset($role_current_user) && $role_current_user == 'anunciante' ){ // apenas anunciantes

    # Inclui um CSS e um HTML ao cabeçalho para o painel
    //add_action('admin_head', 'kadum_add_cabecalho_painel');

    # Inclui um HTML para o RODAPÉ do painel
    //add_filter('admin_footer_text', 'kadum_add_html_rodape_painel');

}

# Remove os Widgets padrões do painel
add_action('admin_menu', 'kadum_remover_widgets_painel');

# Modifica a tela de login do Wordpress
add_action('login_head', 'login_page');
add_filter('login_headerurl', 'link_login_url');
add_filter('login_headertitle', 'link_login_title');


# Adiciona Widgets personalizados no painel
add_action('wp_dashboard_setup', 'kadum_adicionar_widgets_painel');

# Remove itens do menu para anunciantes
add_action('admin_menu', 'kadum_remover_itens_menu');

# Remove a barra de administração do Wordpress
add_filter('show_admin_bar', '__return_false');

# Adiciona javascript ao rodapé que personaliza os status de posts, para anunciantes.
add_action('admin_footer', 'kd_status_posts_javascript');



/**
 * FUNÇÕES DOS GANCHOS
 * -------------------------------------------------------------------
**/


# Inclusão do CSS e HTML para personalização do Painel
function kadum_add_cabecalho_painel(){
    
    # Adiciona apenas, se estiver sendo aberto o painel de administração.
    if( is_admin() ){
    	
    	global $pagenow;

        # CSS
    	wp_enqueue_style('kadum-painel', ab_painel . '/css/estilo.css');

    	# Javascript
    	wp_enqueue_script('kadum_painel_javascript', ab_painel . '/js/javascript.js');

        # Obtendo o grupo ao qual pertence o usuário atual
        global $current_user;
    	$role = array_shift($current_user->roles);

        # Este css é só para anunciantes...
    	if( isset($role) && $role == 'anunciante' ){
    		wp_enqueue_style( 'kadum-painel-anunciantes', ab_painel . '/css/estilo-anunciantes.css' );
    	}

        # Este css aplica regras para o media-new.php que será usado no frame
        # de upload de imagens na criação do anúncio.        
        if( isset($_GET['iframe']) ){
            wp_enqueue_style('kadum_metabox_media_frame', ab_metabox . '/css/media_frame.css');
            wp_enqueue_script('kadum_metabox_media_frame', ab_metabox . '/js/media_frame.js');
        }

    	# Inclui o template
    	include_once(painel . ds . 'template.php');
    }
};


# Adiciona HTML ao rodapé do painel
function kadum_add_html_rodape_painel() {
    
    # Inclui o template
    include_once(painel . ds . 'template_rodape.php');
}


# Personaliza a tela de login
function login_page() {
    
    # Adiciona estilo
    wp_enqueue_style('kadum-login-css', ab_painel . '/css/login.css' );

    # Adicione Javascript também, se quiser!
    #wp_enqueue_script('kadum_login_javascript', ab_painel . '/js/login.js');

    # E caso queira adicionar HTML, ele ficará logo abaixo do <body>
    # Basta dar um echo, ou incluir algum template.
    #echo '<h2>HTML Personalizado na Tela de Login</h2>';
    #include_once(painel . ds . 'template_login.php');
}

# Modifica a URL do link para o site, e não para o Wordpress.org
function link_login_url() { return home_url('/'); }

# Modifica o title do link na tela de login
function link_login_title() { return 'Voltar para a página inicial do ' . get_option('blogname'); }


# Remove os Widgets padrões do painel
function kadum_remover_widgets_painel() {
	
	remove_meta_box('dashboard_right_now', 'dashboard', 'core');        // Widget de "Agora"
	#remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Widget de Comentários
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');   // Incoming Links Widget
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');          // Widget de Plugins

	remove_meta_box('dashboard_quick_press', 'dashboard', 'core');      // Widget do Quick Press
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');    // Widget de Rascunhos Recentes
	remove_meta_box('dashboard_primary', 'dashboard', 'core');          // Widget de RSS do blog do Wordpress.org
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');        // Widget de outras notícias do Wordpress
	
	# Remover widgets específicos de plugins
	remove_meta_box('yoast_db_widget', 'dashboard', 'normal');          // Por exemplo, Widget do Yoast's SEO Plugin
}


# Adiciona Widgets no painel
function kadum_adicionar_widgets_painel(){
	
    # Adiciona o Widget de Estatísticas
    wp_add_dashboard_widget('kadum_painel_widget', 'Widget do Kadum', 'kadum_painel_widget_html');

}


/**
 * FUNÇÕES DE CADA WIDGET
 * ------------------------------------------------------------------
**/

# Widget de estatísticas
function kadum_painel_widget_html() { ?>
	
	<h4>Este é um exemplo de um widget personalizado para o Kadum</h4>

<?php }



# Remove os itens do menu do painel se for um anunciante
function kadum_remover_itens_menu(){
    
    
    global $menu;

    /*** REMOVENDO PARA TODOS ***/

    # Removendo a edição de posts - funcionalidade de blog
    remove_menu_page( 'edit.php' );
    #remove_menu_page( 'tools.php' );

    $menu[60] = array( __('Widgets'), 'switch_themes', 'widgets.php', '', 'menu-top menu-icon-appearance', 'menu-appearance', 'none' );
    $menu[61] = array( __('Menus'), 'switch_themes', 'nav-menus.php', '', 'menu-top menu-icon-appearance', 'menu-appearance', 'none' );
    $menu[66] = array( '', 'read', 'separator3', '', 'wp-menu-separator' );

    # Esta linha remove a página "Media Update" que é usada em segundo plano na removação/atualização
    # de imagens na criação do anúncio. O índice é 12, porque é o que foi definido na função de adição da página
    # que está localizada em <metabox>/registro.php, primeiro hook (gancho)
    unset($menu[12]);


    /*** REMOVENDO APENAS PARA ANUNCIANTES ***/

    # Obtendo o grupo ao qual pertence o usuário atual
    global $current_user;
    $role = array_shift($current_user->roles);

    if( isset($role) && $role == 'anunciante' ){
     
        # Removendo o menu de acesso aos comentários
        #remove_menu_page( 'edit-comments.php' );
     
        # Removendo o menu de Ferramentas
        remove_menu_page( 'tools.php' );
    }


}


# Personalização com Javascript
function kd_status_posts_javascript(){
  global $user_ID;
      
  # Verifica se o usuário é um anunciante
  if(! current_user_can('manage_options') ){

    # Usuário atual
    $current_user = wp_get_current_user();

    # Estes são os status que serão mostrados ao usuário
    $various_status = array('publish','pending','draft','expirado'); 
    
    ?>
        <script type="text/javascript">
        jQuery(function(){
           
            <?php

            # Percorre os status, e mostra ou oculta nos filtros do Wordpress
            foreach( $various_status as $status ):
                $query = new WP_Query(array('post_type' => 'anuncios', 'post_status' => $status, 'author' => $current_user->ID));
            ?>
                <?php if( $query->post_count == 0 ): ?>
                    jQuery("ul.subsubsub li.<?php echo $status; ?>").remove();
                <?php else: ?>
                    jQuery("ul.subsubsub li.<?php echo $status; ?> a .count").html("(<?php echo $query->post_count; ?>)");
                <?php endif; ?>
            <?php endforeach; ?>

            // Remove por padrão estes abaixo
            jQuery("ul.subsubsub li.all").remove();
            jQuery("ul.subsubsub li.mine").remove();
            jQuery("ul.subsubsub li.trash").remove();
            jQuery("ul.subsubsub li.future").remove();

            // Remove a opção de "mover pra lixeira", apenas no frontend,
            // pois, o wordpress já não permite esta ação para anunciantes.
            jQuery('#posts-filter option[value="trash"]').remove();
        });
        </script>
    <?php
    }
}


/**
 * COLUNAS DE ORDEM PERSONALIZADAS PARA ADMINISTRAR OS ANÚNCIOS
 * --------------------------------------------------------------------------
 * Os ganchos aqui, são adicionados próximos a suas respectivas funções.
**/

# Opções do Kadum
$opcoes = get_option('opcoes-kadum');

# Todas colunas disponíveis
if(isset($_GET['post_status']) && ($_GET['post_status'] == 'expirado')){
    $titulo_prazo = 'Expirado em:';
}else{
    $titulo_prazo = 'Expira em:';
}

$colunas_disponiveis = array(
          'titulo' => __('Título'),
        'telefone' => __('Telefone'),
           'plano' => __('Plano'),
           'prazo' => __($titulo_prazo),
         'cliente' => __('Cliente:'),
          'cidade' => __('Cidade'),
    'area_atuacao' => __('Área'),
);

# Colunas que serão exibidas
$colunas = array();

# Usuário atual
$current_user = wp_get_current_user();

if( is_user_logged_in() ){

    # Verifica se o usuário é um anunciante
    if( $current_user->roles == 'anunciante'){

        # Se o usuário é um anunciante, mostrar colunas de acordo com as configurações
        foreach ($opcoes['config_colunas-anunciantes'] as $coluna => $valor){
            
            # Se existe no array, o checkbox da configuração é true
            if( isset($colunas_disponiveis[$coluna]) ){
                
                # Adiciona nas colunas que serão exibidas.
                $colunas[ $coluna ] = $colunas_disponiveis[ $coluna ];
            }
        }

    }else{

        # Caso não seja anunciante, adiciona todas
        $colunas = $colunas_disponiveis;

    }
}


# Organiza os itens da lista para sortear.
add_filter('manage_edit-anuncios_columns', 'kadum_editar_colunas_anuncios');
function kadum_editar_colunas_anuncios( $columns ) {
    
    global $colunas;

    # Esvazia as colunas padrões do Wordpress
    $columns = array('cb' => '<input type="checkbox" />');
    
    # Adiciona as colunas do Kadum
    foreach ($colunas as $key => $value) {
        if($value) $columns[$key] = __($value);
    }

    $columns['comments'] = __( '<img src="' . get_site_url() . '/wp-admin/images/comment-grey-bubble.png" alt="Comentários"></span></span>' );
    $columns['date'] = __( 'Data' );

    return $columns;
};


# Organiza os itens da lista para sortear.
add_action('manage_posts_custom_column', 'kadum_anuncios_colunas', 10, 2 );
function kadum_anuncios_colunas( $column, $post_id ) {
    switch( $column ) {
        
        /* Adiciona coluna para a meta box */
        case 'titulo':
        case 'telefone':
        case 'plano':
        case 'prazo':
            global $campos_mb;
            global $post;
            global $opcoes;
            
            $campos_mb->the_meta();
            
            if($column == 'prazo'){
                $plano   = $campos_mb->get_the_value('plano');
                $prazo   = $opcoes['plano'.$plano.'_periodo'];
                $expira  = date('Y-m-d', strtotime($post->post_date . ' +'.$prazo.'days') );
                $hoje    = date('Y-m-d');
                $nome    = date('d/m/Y', strtotime($post->post_date . ' +'.$prazo.'days') );
                
                if($post->post_status != 'expirado' && (strtotime($hoje) > strtotime($expira))){
                    $post->post_status = 'expirado';
                    wp_update_post($post);
                }

            } 
            elseif($column == 'plano'){

                $plano = $campos_mb->get_the_value('plano');
                $nome = $opcoes['plano'.$plano.'_nome'];
            
            }
            else $nome = $campos_mb->get_the_value($column);

            $out = array();
            $out[] = sprintf( '<a href="%s">%s</a>',
                esc_url( add_query_arg( array( 'post' => $post->ID, 'action' => 'edit' ), 'post.php' ) ),
                esc_html( sanitize_term_field( 'name', $nome, $post->ID, $column, 'display' ) )
            );
            echo join( ', ', $out );

        break;

        /* Adiciona coluna para a taxonomia */
        case 'cliente':
            global $post;
            
            $autor = get_user_by('id', $post->post_author);
            $out = array();
            $out[] = sprintf( '<a href="%s">%s</a>',
                esc_url( add_query_arg( array( 'user_id' => $autor->ID ), 'user-edit.php' ) ),
                esc_html( sanitize_term_field( 'name', $autor->display_name, $post->ID, $column, 'display' ) )
            );
            echo join( ', ', $out );
        break;

        /* Adiciona coluna para a taxonomia */
        case 'cidade':
        case 'area_atuacao':
            global $post;
            $terms = get_the_terms( $post->ID , $column );
            if( is_array($terms) ) {
                $out = array();
                foreach( $terms as $term ) {
                    $out[] = sprintf( '<a href="%1$s">%2$s</a>',
                        esc_url( add_query_arg( array( 'post_type' => $post->post_type, $column => $term->slug ), 'edit.php' ) ),
                        esc_html( sanitize_term_field( 'name', $term->name, $post->ID, $column, 'display' ) )
                    );
                }
                echo join( ', ', $out );
            }else _e('Nenhuma taxonomia');
        
        break;
        default:
            echo 'Nada encontrado.';
        break;
    }
};

# Deixa a Áreas de Atuação como ordenável
add_filter('manage_edit-anuncios_sortable_columns', 'kadum_registro_colunas_areas_atuacao');
function kadum_registro_colunas_areas_atuacao( $columns ){
    
    global $colunas;
    foreach ($colunas as $key => $value) {
        $columns[$key] = $key;
    }
    return $columns;
}

# Executa a Ordenação das Áreas de Atuação
add_filter('requests', 'kadum_ordenacao_areas_atuacao');
function kadum_ordenacao_areas_atuacao( $vars ){
    
    global $campos_mb;
    global $kd_campos;

    if(! isset($vars['orderby']) ) return;

    $vars = array_merge(
        $vars,
        array(
            'meta_key' => $campos_mb->get_the_name($vars['orderby']),
            'orderby' => 'meta_value',
        )
    );
    return $vars;
}

# Mostra apenas posts do Autor, caso seja um anunciante.
add_action('load-edit.php', 'kadum_apenas_anuncios_do_autor' );
function kadum_apenas_anuncios_do_autor() {
    global $user_ID;
        
    # Verifica se o usuário é um anunciante
    if(! current_user_can('manage_options') ){      
        if ( ! isset( $_GET['author'] ) || $_GET['author'] != $user_ID ) {
            wp_redirect( add_query_arg( 'author', $user_ID ) );
            exit;
        }
    }

}