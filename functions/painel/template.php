<link rel="shortcut icon" href="<?php echo kadum_tema_url . '/img/ico/favicon.ico' ?>" />

<?php if(! isset($_GET['iframe']) ): ?>
<div id="kadum_painel_cabecalho">
	
	<div id="logo-painel">
        <a class="logo" href="<?php echo home_url(); ?>"></a>
		<h2><a href="<?php echo admin_url(); ?>">Painel Administrativo</a></h2>
	</div>

    <?php global $user_login; ?>

    <div id="card-usuario">
        <?php if( is_user_logged_in() ): ?>

        <?php global $current_user; get_currentuserinfo(); ?>

        <div class="foto">
            <img src="<?php echo get_avatar_url(get_avatar( $current_user->ID, 40 )); ?>" class="block img-polaroid"/>
        </div>

        <nav class="nav">
            <a class="display_name" href="<?php echo get_author_posts_url($current_user->ID); ?>" title="Ver meus anúncios"><?php echo $current_user->display_name; ?></a>
            <a href="<?php echo esc_url( admin_url('profile.php') ); ?>">Editar perfil</a>
            <span>|</span>
            <a href="<?php echo wp_logout_url( home_url('/') ); ?>">Sair &raquo;</a>
            <!--<li><a href="<?php echo esc_url( admin_url('profile.php') ); ?>">Editar meu perfil</a></li>
            <li><a href="<?php echo get_author_posts_url($current_user->ID); ?>">Meus anúncios</a></li>
            <li><a href="<?php echo esc_url( admin_url('post-new.php?post_type=anuncios') ); ?>">Criar anúncio</a></li>
            <li><a href="<?php echo wp_logout_url( home_url('/') ); ?> ">Sair</a></li>-->
        </nav>

    	<?php endif; ?>
    </div>
</div>

<div id="kadum_painel_toolbar">
	<a href="/">Voltar ao site</a>
</div>
<?php endif; ?>