<?php if(! isset($_GET['iframe']) ): ?>
<div id="kadum_painel_rodape">
	
	<p>Rodapé personalizado do Kadum!!!</p>

  <div class="rodape-bottom">
    <p class="pull-left">&copy; Copyright <?php echo date('Y'); ?> - Todos os direitos reservados - Kadum.com.br</p>
    <p class="pull-right">Orgulhosamente criado com o <a href="http://www.wordpress.org/" target="_blank">Wordpress</a> <?php echo get_bloginfo('version'); ?></p>
  </div>
</div>
<?php endif; ?>