﻿/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * 
 * no arquivo docs.pdf no root do tema.
 * 
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * 
 * 
 * Continue a arte: código é poesia!
 */




/**
 * Esta folha de estilo personaliza vários pontos do painel do Wordpress.
 * Cada ponto está comentado e explicado para que se possa entender a estilização.
**/


jQuery(document).ready(function(){

    jQuery('#screen-options-link-wrap').click(function(){
        
        jQuery(this).toggleClass('screen-meta-active');

    });
});