<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Aqui é adicionado o novo grupo de usuários "Anunciantes"
 * Também são adicionados alguns campos ao registro de anunciante.
**/


/*** GANCHOS ***/

add_action('admin_init', 'kadum_add_grupo_anunciantes');

add_filter('user_contactmethods', 'kadum_campos_usuario', 10, 1);

add_action('admin_footer', 'javascript_ocultar_inputs');


/*** FUNÇÕES DOS GANCHOS ***/


# Adiciona novo grupo de usuários, caso não exista ainda.
function kadum_add_grupo_anunciantes(){
	
	$anunciante = get_role('anunciante');

	if( empty($anunciante) ){
		add_role(
			'anunciante',
			'Anunciante',
			array(
				'read' => true,
				'edit_posts' => true,
				'edit_private_posts' => true,
				'edit_published_posts' => true,
				'edit_others_posts' => true,
			)
		);
	}
}



# Adiciona as novas opções de inputs
function kadum_campos_usuario( $contatos ){
	 
	# Caso queira adicionar mais campos aos perfis de
	# usuarios anunciantes, basta seguir o exemplo abaixo:
	#$contatos['telefone_comercial'] = 'Telefone Comercial';
	 
	# Remove campos
	unset($contatos['aim']);
	unset($contatos['yim']);
	unset($contatos['jabber']);
	
	return $contatos;
}



# Adiciona javascript que remove os primeiros inputs
# da edição de perfil, apenas para anunciantes.
function javascript_ocultar_inputs(){
  global $pagenow;

  if(! current_user_can('manage_options') && ( $pagenow == 'profile.php' ) ){ ?>
		<script type="text/javascript">
			jQuery(function(){
				var h3 = jQuery("#your-profile h3:first");
				h3.next('.form-table').remove();
				h3.remove();
			});
		</script>
	<?php
	}
}



/*** ALTERA O REMENTENTE DE EMAIL ***/
/*
add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');
 
function new_mail_from($old) {
	return 'contato@kadum.com.br';
}

function new_mail_from_name($old) {
 return 'Kadum.com.br';
}*/