<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Este arquivo serve como controlador do page_criar-conta.php
 * Não há muito o que fazer aqui, já que só é necessário instanciar
 * a classe, que cuida do resto.
**/

# Classe com funções para uso no Kadum
if(!class_exists('KadumCadastro')) require_once( usuarios . '/kadumcadastro.class.php' );

$cadastro = new KadumCadastro();