<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Classe que manipula o cadastro de usuários do Kadum.
 * 
 * Google = https://developers.google.com/accounts/docs/OAuth2
 * Google = https://developers.google.com/accounts/docs/OAuth2Login
 * Twitter = https://github.com/abraham/twitteroauth 
**/


class KadumCadastro {

  public $nome    = null;
  public $email   = null;
  public $usuario = null;
  public $erros   = array();
  
  /**
   * Indica o status do cadastro.
   * Passa a ser true quando o cadastro é efetuado com sucesso.
  **/
  public $nova_conta = false;

  function __construct(){

    if( isset($_POST['email_modo']) ) $this->email();
    
    elseif( isset($_GET['facebook']) ) $this->facebook();
    
    elseif( isset($_GET['twitter']) ) $this->twitter();

    elseif( isset($_GET['google']) ) $this->google();

  }



  /**
   * EMAIL!!!
   * 
  **/
  private function email(){
    
    if( isset($_POST['nome']) ) $this->nome = $_POST['nome'];
    if( isset($_POST['usuario']) ) $this->usuario = $_POST['usuario'];
    if( isset($_POST['email']) ) $this->email = $_POST['email'];

    $this->finalizaCadastro();

  }


  /**
   * Integração com o Facebook
   * 
  **/
  private function facebook(){
    
    # Inclui a biblioteca do Facebook
    include_once(usuarios . ds . 'facebook' . ds . 'facebook.php');

    # Cria uma instância da aplicação do Facebook, usando as credenciais.
    $facebook = new Facebook(array(
      'appId'  => '414232121972623',
      'secret' => 'c533d48e9c062e326924bff76ecb9c91',
    ));

    # Obtém ID do usuário
    $fb_user = $facebook->getUser();

    # Se existir $fb_user aqui, é sinal que temos um usuário logado.
    # Sendo assim, pegar dados deste usuário logado.
    if ($fb_user) {
      try {
        $perfil = $facebook->api('/me?fields=username,name,email');
      } catch (FacebookApiException $e) {
        error_log($e);
        $fb_user = null;
      }
    }

    # Login ou logout dependendo do usuário estar logado.
    if ($fb_user) $logoutUrl = $facebook->getLogoutUrl();
    else header("Location: " . $facebook->getLoginUrl(array('scope' => 'email')) );

    # Nome, username e ID já são públicos por padrão, não é preciso solicitar permissão.
    $this->nome = $perfil['name'];
    $this->usuario = $perfil['username'];
    

    # Logo, verifica-se apenas se o usuário liberou acesso ao email.
    if(! isset($perfil['email']) ){

      # Se ele não liberou, mas já digitou algum após o erro ser mostrado, pegue este email.
      if( isset($_POST['email_substituto_fb']) ){
        $this->email = $_POST['email_substituto_fb'];
      }else{

        $this->erros['api_erro'] = array();

        $loginUrl = $facebook->getLoginUrl(array('scope' => 'email'));

        $this->erros['api_erro']['titulo'] = 'Libere acesso ao seu email';
        $this->erros['api_erro']['conteudo'] = '<p>Precisamos que você libere o acesso ao seu email no Facebook, para que possamos finalizar o cadastro.</p>';
        $this->erros['api_erro']['conteudo'] .= '<a class="btn btn-primary btn-large btn-block margin-bottom-30" href="' . $loginUrl . '">Liberar acesso no facebook</a>';
        $this->erros['api_erro']['conteudo'] .= '<p class="align-center">Ou, se preferir, digite um email válido abaixo:</p>';
        $this->erros['api_erro']['conteudo'] .= '<input type="hidden" name="facebook" value="✓" />';
        $this->erros['api_erro']['conteudo'] .= '<input type="text" class="input-block-level" name="email_substituto_fb" placeholder="Por exemplo: joaojose@gmail.com" />';
        $this->erros['api_erro']['conteudo'] .= '<input class="btn btn-block" type="submit" value="Usar este email">';
      }

    }else{
      $this->email = $perfil['email'];
    };

    # Finaliza o cadastro
    $this->finalizaCadastro();
  }


  /**
   * Integração com o Twitter
   * 
  **/
  private function twitter(){
    
    # Inclui a biblioteca do Twitter
    require_once(usuarios.ds.'twitter'.ds.'twitteroauth.class.php');

    # Inicia uma sessão
    session_start();

    $consumer_key = 'MZcoP4bb5GuB9sHL4YPXg';
    $consumer_secret = 'cghx2uIzcWmX4dfbQjLYbPDx9GxttKZblW3f4fN7opc';
    $callback_url = get_permalink().'?twitter';

    # Se o usuário já digitou o email, que bom, pegue-o.
    if( isset($_POST['email_substituto_tw']) ){
      $this->nome = $_SESSION['twitter_name'];
      $this->usuario = $_SESSION['twitter_screen_name'];
      $this->email = $_POST['email_substituto_tw'];
    }

    # Verifica se já há um token e um token_secret
    elseif( !isset($_SESSION['oauth_token']) && !isset($_SESSION['oauth_token_secret']) ){

      # Instacia a classe com as credenciais
      $connection = new TwitterOAuth($consumer_key, $consumer_secret);

      # Obtém os tokens de acesso temporários
      $request_token = $connection->getRequestToken($callback_url);

      # Salva os tokens temporários na sessão
      $_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
      $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

      # Verifica o HTTP code da resposta para redirecionar o usuário ou exibir o erro correto.
      switch ($connection->http_code) {
        case 200:
          # Redireciona para a tela de login do Twitter
          header('Location: ' . $connection->getAuthorizeURL($token));
          break;
        default:
          $this->erros['api_erro'] = array();
          $this->erros['api_erro']['titulo'] = 'Não foi possível se conectar ao Twitter.';
          $this->erros['api_erro']['conteudo'] = '<p>Não estamos conseguindo nos conectar com o Twitter. Tente novamente mais tarde.</p>';
          $this->erros['api_erro']['conteudo'] .= '<a class="btn btn-primary btn-large btn-block" href="' . get_permalink() . '">Ok, voltar &raquo;</a>';
      }

    }

    # Agora é só pegar as informações
    else{

      # Se o token de acesso é antigo, recomeçar o processo.
      if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
        $_SESSION['oauth_status'] = 'oldtoken';
        header('Location: ' . $callback_url);
      }

      # Instancia a classe novamente utilizando os novos token_key e token_secret
      $connection = new TwitterOAuth($consumer_key, $consumer_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

      # Requisita o acesso as informações ao twitter
      $access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);

      # Remove os tokens antigos
      unset($_SESSION['oauth_token']);
      unset($_SESSION['oauth_token_secret']);

      # Se o http_code não é igual a 200, ocorreu algum erro.
      if ($connection->http_code != 200) {
        $this->erros['api_erro'] = array();
        $this->erros['api_erro']['titulo'] = 'Não foi possível se conectar ao Twitter.';
        $this->erros['api_erro']['conteudo'] = '<p>Não estamos conseguindo nos conectar com o Twitter. Tente novamente mais tarde.</p>';
        $this->erros['api_erro']['conteudo'] .= '<a class="btn btn-primary btn-large btn-block" href="' . get_permalink() . '">Ok, voltar &raquo;</a>';
      }

      # Obtém as informações do usuário logado.
      $perfil = $connection->get('account/verify_credentials');
    
      # Com a API do twitter, só é possível obter nome e username.
      # Logo, será preciso perguntar o email ao usuário.
      # Então, vamos salvar essas informações na sessão,
      # e quando o usuário digitar o email, teremos tudo em ordem.
      $_SESSION['twitter_name'] = $perfil->name;
      $_SESSION['twitter_screen_name'] = $perfil->screen_name;

      $this->erros['api_erro'] = array();
      $this->erros['api_erro']['titulo'] = $perfil->name . ', precisamos do seu email.';
      $this->erros['api_erro']['conteudo'] = '<p>Para finalizar o seu cadastro agora é muito simples, basta digitar o seu email.</p>';
      $this->erros['api_erro']['conteudo'] .= '<input type="hidden" name="twitter" value="✓" />';
      $this->erros['api_erro']['conteudo'] .= '<input type="text" class="input-block-level" name="email_substituto_tw" placeholder="Por exemplo: joaojose@gmail.com" />';
      $this->erros['api_erro']['conteudo'] .= '<input class="btn btn-block" type="submit" value="Pronto, finalizar cadastro.">';
    }

    # Finaliza o cadastro
    $this->finalizaCadastro();    
  }


  /**
   * Integração com o Google
   * 
  **/
  private function google(){

    # Inclue as bibliotecas do Google
    require usuarios . ds . 'google' . ds . 'apiclient.php';
    require usuarios . ds . 'google' . ds . 'contrib' . ds . 'apiOauth2Service.php';
             
    # Instancia a classe
    $cliente = new apiClient();

    # Visite https://code.google.com/apis/console para gerar seu
    # oauth2_client_id, oauth2_client_secret, e registrar seu oauth2_redirect_uri.
    $cliente->setClientId('685502729704.apps.googleusercontent.com');
    $cliente->setClientSecret('8yvbgaZwpVZLeztRJxGUTJnm');
    $cliente->setRedirectUri( get_permalink() . '?google=✓');
    $cliente->setScopes(array('profile', 'email'));

    $oauth2 = new apiOauth2Service($cliente);

    if( isset($_GET['code']) ){
      try{
        $cliente->authenticate($_GET['code']);

        $access_token = $cliente->getAccessToken();
        $cliente->setAccessToken($access_token);

        $perfil = $oauth2->userinfo->get();
        $this->nome = $perfil['name'];
        $this->usuario = preg_replace('/[^A-Za-z0-9]/', "", $perfil['name']);
        $this->email = $perfil['email'];
      
      }catch(Exception $e){
        $this->erros['api_erro'] = array();
        $this->erros['api_erro']['titulo'] = 'Ops, há algo errado.';
        $this->erros['api_erro']['conteudo'] = '<p>' . $e->getMessage() . '</p>';
        $this->erros['api_erro']['conteudo'] .= '<a class="btn btn-primary btn-block" href="' . get_permalink() . '">Ok, voltar &raquo;</a>';
      
      }

    }else header('Location: ' . $cliente->createAuthUrl() );

    $this->finalizaCadastro();
  }

  # Você pode criar mais um modo de cadastro, utilizando alguma outra API.
  # private function exemplo_de_modo(){
  #   // importe a api que for utilizar..
  #   // trate os dados necessários..
  #   // atribua os valores a $this->nome, $this->usuario e $this->email
  #   // finalize o cadastro chamando $this->finalizaCadastro();
  # }

  private function finalizaCadastro(){

    $this->nova_conta = true;

    if( strlen($this->nome) < 3 || strlen($this->nome) > 16 )
    $this->erros[] = 'Seu nome deve ter entre 3 e 16 caracteres.';

    if( strlen($this->usuario) < 3 || strlen($this->usuario) > 16 )
    $this->erros[] = 'Seu nome de usuário deve ter entre 3 e 16 caracteres.';

    elseif( username_exists($this->usuario) )
    $this->erros[] = 'Esse nome de usuário já existe!';
   
    if( !filter_var($this->email, FILTER_VALIDATE_EMAIL) )
    $this->erros[] = 'Você deve digitar um email válido.';

    elseif( email_exists($this->email) )
    $this->erros[] = 'Esse email já está sendo usado!';
   
    if(! count($this->erros) ){

      # Todo usuário precisa de uma senha...
      $senha = wp_generate_password();
      
      # Vamos cadastrá-lo então!
      $novo_usuario = array();
      $novo_usuario['role'] = 'anunciante';
      $novo_usuario['display_name'] = esc_attr( $this->nome );
      $novo_usuario['user_login'] = esc_attr( $this->usuario );
      $novo_usuario['user_email'] = esc_attr( $this->email );
      $novo_usuario['user_pass'] = $senha;

      # Dados definidos? Completa a ficha!
      $this->nova_conta = wp_insert_user( $novo_usuario );
      wp_new_user_notification($this->nova_conta, $senha);
      
      # Se deu erro no cadastro, vamos avisar ao usuário!
      if( is_wp_error($this->nova_conta) ) $cadastro->erros[] = 'Erro no cadastro.';

      # Agora vamos fazer o login...
      $credenciais = array();
      $credenciais['user_login'] = $this->usuario;
      $credenciais['user_password'] = $senha;
      $credenciais['remember'] = true;
      $login = wp_signon( $credenciais, false );

      # Se deu erro no login, não precisamos dizer
      # que o usuário já pode criar anúncios.
      # Porém, a conta já foi criada!
      if(is_wp_error($login)) $cadastro->erros['error_login'] = $login->get_error_message();
    }
  }



}