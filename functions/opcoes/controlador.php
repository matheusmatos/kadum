<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Aqui é criada a página de opções do Kadum.
 * Para a criação desta página foi utilizado o framework NHP Theme Options. Detalhes:
 * 
 * @author  leemason
 * @package NHP-Theme-Options-Framework
 * @version 1.0.6
 * @link    https://github.com/leemason/NHP-Theme-Options-Framework.git
**/
if(!class_exists('NHP_Options')) require_once( opcoes . '/nhpframework.class.php' );



/**
 * ATENÇÃO: as funções que facilitam Child Themes (temas filho) adicionar e/ou remover opções do tema pai, foram removidas.
 * Caso seja preciso, é possível obter estas funções com uma versão atualizada do framework.
 * Obs.: (Transcreva as funções. Em caso de substituição completa, as opções já definidas serão perdidas.)
**/



/*** GANCHOS ***/

# Executa a função no gancho init. Consultar o gancho 'init' do Wordpress
add_action('init', 'setup_opcoes_kadum', 0);



/**
 * Aqui começa a criação da página de opções.
 * Para substituir valores padrões, descomente as linhas
**/

function setup_opcoes_kadum(){
	
	$args = array();

	# Defina como true para visualizar a classe de configurações/info na página de opções.
	$args['dev_mode'] = true;

	# google_api_key deve ser definido se você quiser usar o Google Web Fonts
	# $args['google_api_key'] = '***';

	# Caso precise substituir a folha de estilo padrão, certifique-se de linkar outra, senão a página vira uma bagunça!
	# $args['stylesheet_override'] = true;

	# Adicionando HTML antes do formulário
	$args['intro_text'] = __('<p>Essas são as opções do Kadum. Você pode definir configurações dos planos, dos loops da tela inicial, entre outras configurações.</p>', 'kadum-opts');

	# Marque para desativar a funcionalidade de exportar/importar
	$args['show_import_export'] = false;

	# Escolha um nome para as opções do seu tema.
	$args['opt_name'] = 'opcoes-kadum';

	# Ícone do menu personalizado
	$args['menu_icon'] = '';

	# Título do menu personalizado
	$args['menu_title'] = __('Opções', 'kadum-opts');

	# Título personalizado para a página de opções.
	$args['page_title'] = __('Opções do Kadum', 'kadum-opts');

	# Slug personalizado para a página de opções (wp-admin/themes.php?page=***)
	$args['page_slug'] = 'opcoes-kadum';

	# Capacidade personalizada para a página de opções - o padrão é "manage_options"
	# $args['page_cap'] = 'manage_options';

	# Local personalizado na sequencia do menu - deverá ser único ou sobreescreverá outros itens do menu.
	$args['page_position'] = 71;

	# ID CSS do ícone da página. (usado para substituir o ícone da página ao lado do título)
	$args['page_icon'] = 'icon-generic';

	# Deseja desativar a visualização dos subitens no menu de opções? descomente esta linha
	# $args['allow_sub_menu'] = false;

	# A classe nos ajudou muito, mas é necessário retirar os créditos do rodapé
	$args['footer_credit'] = '';


	# Consultando as opções já salvas, para ser usada, por exemplo, nos nomes dos planos.
	$opcoes = get_option( $args['opt_name'] );
	
	#Adicionando as abas e opções, propriamente ditas.
	$sections = array();


	/**
	 * Plano 1 (free)
	**/
	$sections[] = array(
		'title' => __('Plano: ' . $opcoes['plano1_nome'], 'kadum-opts'),
		'desc' => __('<p class="description">Estas são as configurações do primeiro plano do Kadum</p>', 'kadum-opts'),
		'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_088_adress_book.png',
		'fields' => array(
			
			/**
			 * Os IDS de cada campo devem ser únicos.
			 * O padrão adotado é nomedaaba_campo.
			**/

			# Nome do plano
			array(
				'id' => 'plano1_nome',
				'type' => 'text',
				'title' => __('Nome do plano', 'kadum-opts'),
				'sub_desc' => __('Escolha um nome atraente para que os seus anunciantes possam decidir qual plano utilizar.', 'kadum-opts'),
				'desc' => __('Não utilize html.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Você não pode utilizar HTML neste campo. Todas as tags HTML foram removidas.',
				'std' => 'Free'
			),

			# Preço
			array(
				'id' => 'plano1_preco',
				'type' => 'text',
				'title' => __('Preço', 'kadum-opts'),
				'sub_desc' => __('Defina um preço para este plano.', 'kadum-opts'),
				'desc' => __("Utilize apenas numerais, e caso o plano seja gratuito, deixe-o como '0'.", 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize decimais, como por exemplo: 49.90 / 50 / 90.00 ',
				'std' => '0',
				'class' => 'small-text'
			),

			# Período
			array(
				'id' => 'plano1_periodo',
				'type' => 'text',
				'title' => __('Período', 'kadum-opts'),
				'sub_desc' => __('Defina um período para que um anúncio deste plano expire.', 'kadum-opts'),
				'desc' => __('Utilize apenas números, referente aos dias. ', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade de dias do prazo do plano.',
				'std' => '10',
				'class' => 'small-text'
			),

			# Número de caracteres
			array(
				'id' => 'plano1_descricao_maximo',
				'type' => 'text',
				'title' => __('Máximo de caracteres', 'kadum-opts'),
				'sub_desc' => __('Defina um número máximo de caracteres para a descrição do anúncio deste plano.', 'kadum-opts'),
				'desc' => __('Utilize apenas números, referente ao número de caracteres.', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade máxima de caracteres na descrição do anúncio.',
				'std' => '300',
				'class' => 'small-text'
			),

			# Número de Fotos
			array(
				'id' => 'plano1_nfotos',
				'type' => 'text',
				'title' => __('Quantidade de Fotos', 'kadum-opts'),
				'sub_desc' => __('Quantas fotos os anúncios deste plano poderá ter?', 'kadum-opts'),
				'desc' => __('Defina a quantidade de fotos utilizando um número.', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade de fotos do plano.',
				'std' => '1',
				'class' => 'small-text'
			),	

			# Vantagens
			array(
				'id' => 'plano1_vantagens',
				'type' => 'multi_text',
				'title' => __('Vantagens', 'kadum-opts'),
				'sub_desc' => __('Deixe claro para seus anúnciantes quais as vantagens ele terá com este plano.', 'kadum-opts'),
				'desc' => __('Adicione uma vantagem por linha, HTML não é permitido.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Não utilize HTML nas vantagens. Todas tags HTML foram removidas.',
				'std' => array('Nome','Telefone','Descrição','Estatísticas','1 foto'),
			),	

			# Permitir Endereço
			array(
				'id' => 'plano1_endereco',
				'type' => 'checkbox',
				'title' => __('Permitir endereço?', 'kadum-opts'), 
				'sub_desc' => __('Os anúncios deste plano poderão informar o endereço?', 'kadum-opts'),
				'desc' => __('Deixe marcado para permitir endereço nos anúncios do plano.', 'kadum-opts'),
				'std' => '0' // 1 = on | 0 = off
			),	

			# Permitir Mapa
			array(
				'id' => 'plano1_mapa',
				'type' => 'checkbox',
				'title' => __('Permitir mapa?', 'kadum-opts'), 
				'sub_desc' => __('O mapa deverá ser exibido nos anúncios deste plano?', 'kadum-opts'),
				'desc' => __('Deixe marcado para exibir o mapa nos anúncios do plano.', 'kadum-opts'),
				'std' => '0' // 1 = on | 0 = off
			),
		)
	);



	/**
	 * Plano 2 (basic)
	**/
	$sections[] = array(
		'title' => __('Plano: ' . $opcoes['plano2_nome'], 'kadum-opts'),
		'desc' => __('<p class="description">Estas são as configurações do segundo plano do Kadum</p>', 'kadum-opts'),
		'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_088_adress_book.png',
		'fields' => array(
			
			/**
			 * Os IDS de cada campo devem ser únicos.
			 * O padrão adotado é nomeaba_campo.
			**/

			# Nome do plano
			array(
				'id' => 'plano2_nome',
				'type' => 'text',
				'title' => __('Nome do plano', 'kadum-opts'),
				'sub_desc' => __('Escolha um nome atraente para que os seus anunciantes possam decidir qual plano utilizar.', 'kadum-opts'),
				'desc' => __('Não utilize html.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Você não pode utilizar HTML neste campo. Todas as tags HTML foram removidas.',
				'std' => 'Basic'
			),

			# Preço
			array(
				'id' => 'plano2_preco',
				'type' => 'text',
				'title' => __('Preço', 'kadum-opts'),
				'sub_desc' => __('Defina um preço para este plano.', 'kadum-opts'),
				'desc' => __("Utilize apenas numerais, e caso o plano seja gratuito, deixe-o como '0'.", 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize decimais, como por exemplo: 49.90 / 50 / 90.00 ',
				'std' => '50',
				'class' => 'small-text'
			),

			# Período
			array(
				'id' => 'plano2_periodo',
				'type' => 'text',
				'title' => __('Período', 'kadum-opts'),
				'sub_desc' => __('Defina um período para um anúncio deste plano expire.', 'kadum-opts'),
				'desc' => __('Utilize apenas números, referente aos dias. ', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade de dias do prazo do plano.',
				'std' => '20',
				'class' => 'small-text'
			),

			# Número de caracteres
			array(
				'id' => 'plano2_descricao_maximo',
				'type' => 'text',
				'title' => __('Máximo de caracteres', 'kadum-opts'),
				'sub_desc' => __('Defina um número máximo de caracteres para a descrição do anúncio deste plano.', 'kadum-opts'),
				'desc' => __('Utilize apenas números, referente ao número de caracteres.', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade máxima de caracteres na descrição do anúncio.',
				'std' => '400',
				'class' => 'small-text'
			),

			# Número de Fotos
			array(
				'id' => 'plano2_nfotos',
				'type' => 'text',
				'title' => __('Quantidade de Fotos', 'kadum-opts'),
				'sub_desc' => __('Quantas fotos os anúncios deste plano poderá ter?', 'kadum-opts'),
				'desc' => __('Defina a quantidade de fotos utilizando um número.', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade de fotos do plano.',
				'std' => '5',
				'class' => 'small-text'
			),	

			# Vantagens
			array(
				'id' => 'plano2_vantagens',
				'type' => 'multi_text',
				'title' => __('Vantagens', 'kadum-opts'),
				'sub_desc' => __('Deixe claro para seus anúnciantes quais as vantagens ele terá com este plano.', 'kadum-opts'),
				'desc' => __('Adicione uma vantagem por linha, HTML não é permitido.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Não utilize HTML nas vantagens. Todas tags HTML foram removidas.',
				'std' => array('Nome','Telefone','Descrição','Estatísticas','5 fotos','Endereço'),
			),	

			# Permitir Endereço
			array(
				'id' => 'plano2_endereco',
				'type' => 'checkbox',
				'title' => __('Permitir endereço?', 'kadum-opts'), 
				'sub_desc' => __('Os anúncios deste plano poderão informar o endereço?', 'kadum-opts'),
				'desc' => __('Deixe marcado para permitir endereço nos anúncios do plano.', 'kadum-opts'),
				'std' => '1' // 1 = on | 0 = off
			),	

			# Permitir Mapa
			array(
				'id' => 'plano2_mapa',
				'type' => 'checkbox',
				'title' => __('Permitir mapa?', 'kadum-opts'), 
				'sub_desc' => __('O mapa deverá ser exibido nos anúncios deste plano?', 'kadum-opts'),
				'desc' => __('Deixe marcado para exibir o mapa nos anúncios do plano.', 'kadum-opts'),
				'std' => '0' // 1 = on | 0 = off
			),
		)
	);



	/**
	 * Plano 3 (premium)
	**/
	$sections[] = array(
		'title' => __('Plano: ' . $opcoes['plano3_nome'], 'kadum-opts'),
		'desc' => __('<p class="description">Estas são as configurações do terceiro plano do Kadum</p>', 'kadum-opts'),
		'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_088_adress_book.png',
		'fields' => array(
			
			/**
			 * Os IDS de cada campo devem ser únicos.
			 * O padrão adotado é nomeaba_campo.
			**/

			# Nome do plano
			array(
				'id' => 'plano3_nome',
				'type' => 'text',
				'title' => __('Nome do plano', 'kadum-opts'),
				'sub_desc' => __('Escolha um nome atraente para que os seus anunciantes possam decidir qual plano utilizar.', 'kadum-opts'),
				'desc' => __('Não utilize html.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Você não pode utilizar HTML neste campo. Todas as tags HTML foram removidas.',
				'std' => 'Premium'
			),

			# Preço
			array(
				'id' => 'plano3_preco',
				'type' => 'text',
				'title' => __('Preço', 'kadum-opts'),
				'sub_desc' => __('Defina um preço para este plano.', 'kadum-opts'),
				'desc' => __("Utilize apenas numerais, e caso o plano seja gratuito, deixe-o como '0'.", 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize decimais, como por exemplo: 49.90 / 50 / 90.00 ',
				'std' => '90',
				'class' => 'small-text'
			),

			# Período
			array(
				'id' => 'plano3_periodo',
				'type' => 'text',
				'title' => __('Período', 'kadum-opts'),
				'sub_desc' => __('Defina um período para um anúncio deste plano expire.', 'kadum-opts'),
				'desc' => __('Utilize apenas números, referente aos dias. ', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade de dias do prazo do plano.',
				'std' => '30',
				'class' => 'small-text'
			),

			# Número de caracteres
			array(
				'id' => 'plano3_descricao_maximo',
				'type' => 'text',
				'title' => __('Máximo de caracteres', 'kadum-opts'),
				'sub_desc' => __('Defina um número máximo de caracteres para a descrição do anúncio deste plano.', 'kadum-opts'),
				'desc' => __('Utilize apenas números, referente ao número de caracteres.', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade máxima de caracteres na descrição do anúncio.',
				'std' => '500',
				'class' => 'small-text'
			),

			# Número de Fotos
			array(
				'id' => 'plano3_nfotos',
				'type' => 'text',
				'title' => __('Quantidade de Fotos', 'kadum-opts'),
				'sub_desc' => __('Quantas fotos os anúncios deste plano poderá ter?', 'kadum-opts'),
				'desc' => __('Defina a quantidade de fotos utilizando um número.', 'kadum-opts'),
				'validate' => 'numeric',
				'msg' => 'Utilize apenas números, referente a quantidade de fotos do plano.',
				'std' => '10',
				'class' => 'small-text'
			),	

			# Vantagens
			array(
				'id' => 'plano3_vantagens',
				'type' => 'multi_text',
				'title' => __('Vantagens', 'kadum-opts'),
				'sub_desc' => __('Deixe claro para seus anúnciantes quais as vantagens ele terá com este plano.', 'kadum-opts'),
				'desc' => __('Adicione uma vantagem por linha, HTML não é permitido.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Não utilize HTML nas vantagens. Todas tags HTML foram removidas.',
				'std' => array('Nome','Telefone','Descrição','Estatísticas','5 fotos','Endereço','Mapa'),
			),	

			# Permitir Endereço
			array(
				'id' => 'plano3_endereco',
				'type' => 'checkbox',
				'title' => __('Permitir endereço?', 'kadum-opts'), 
				'sub_desc' => __('Os anúncios deste plano poderão informar o endereço?', 'kadum-opts'),
				'desc' => __('Deixe marcado para permitir endereço nos anúncios do plano.', 'kadum-opts'),
				'std' => '1' // 1 = on | 0 = off
			),	

			# Permitir Mapa
			array(
				'id' => 'plano3_mapa',
				'type' => 'checkbox',
				'title' => __('Permitir mapa?', 'kadum-opts'), 
				'sub_desc' => __('O mapa deverá ser exibido nos anúncios deste plano?', 'kadum-opts'),
				'desc' => __('Deixe marcado para exibir o mapa nos anúncios do plano.', 'kadum-opts'),
				'std' => '1' // 1 = on | 0 = off
			),
		)
	);



	/**
	 * Outras configurações
	**/
	$sections[] = array(
		'title' => __('Outras configurações', 'kadum-opts'),
		'desc' => __('<p class="description">Configurações diversas do Kadum</p>', 'kadum-opts'),
		'icon' => NHP_OPTIONS_URL.'img/glyphicons/glyphicons_023_cogwheels.png',
		'fields' => array(

			# Colunas para os anunciantes
			array(
				'id' => 'config_colunas-anunciantes',
				'type' => 'multi_checkbox',
				'title' => __('Colunas para os anunciantes', 'kadum-opts'),
				'sub_desc' => __('Colunas a serem exibidas para o anunciante.', 'kadum-opts'),
				'desc' => __('</br>Marque apenas as opções de ordenação que você quer exibir ao anunciante.', 'kadum-opts'),
				'options' => array(
					'titulo' => 'Título',
					'telefone' => 'Telefone',
					'plano' => 'Plano',
					'prazo' => 'Expira em',
					'cliente' => 'Cliente',
					'cidade' => 'Cidade',
					'area_atuacao' => 'Área de Atuação'
				),
				'std' => array(
					'titulo'       => '1',
					'telefone'     => '1',
					'plano'        => '1',
					'prazo'        => '1',
					'cliente'      => '1',
					'cidade'       => '1',
					'area_atuacao' => '1'
				)
			),		
			
			# Token do Google Maps
			array(
				'id' => 'config_gmaps_token',
				'type' => 'text',
				'title' => __('Token do Google Maps', 'kadum-opts'),
				'sub_desc' => __('Defina aqui o token utilizado na integração com o Google Maps.', 'kadum-opts'),
				'desc' => __('</br>Cuidado! Se o token estiver preenchido incorretamente, a integração com o Google Maps poderá não funcionar.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Você não pode utilizar HTML neste campo. Todas as tags HTML foram removidas.',
				'std' => 'AIzaSyA9H9bzIcgBTdED2uqWNdvW0b5zn08Px8Y'
			),		
			
			# Coordenadas geográficas padrão
			array(
				'id' => 'config_coordenadas',
				'type' => 'text',
				'title' => __('Coordenadas padrão', 'kadum-opts'),
				'sub_desc' => __('Defina aqui as coordenads geográficas padrão para mostrar no mapa dos anúncios. Separe latitude e longitude por uma vírgula, respectivamente.', 'kadum-opts'),
				'desc' => __('</br>Por exemplo: -15.559544,-56.093445', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Você não pode utilizar HTML neste campo. Todas as tags HTML foram removidas.',
				'std' => '-15.559544,-56.093445'
			),		
			
			# Token do Pagseguro
			array(
				'id' => 'config_pagseguro_token',
				'type' => 'text',
				'title' => __('Token do Pagseguro', 'kadum-opts'),
				'sub_desc' => __('Defina aqui o token utilizado na integração com o PagSeguro.', 'kadum-opts'),
				'desc' => __('</br>Cuidado! Se o token estiver preenchido incorretamente, a integração com o pagseguro não funcionará.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Você não pode utilizar HTML neste campo. Todas as tags HTML foram removidas.',
				'std' => '223B145FF8E943249755C4C2AB73D57E'
			),

			# Email do Pagseguro
			array(
				'id' => 'config_pagseguro_email',
				'type' => 'text',
				'title' => __('Email do Pagseguro', 'kadum-opts'),
				'sub_desc' => __('Defina aqui o email da conta utilizada na integração com o PagSeguro.', 'kadum-opts'),
				'desc' => __('</br>Cuidado! Se o email estiver preenchido incorretamente, a integração com o pagseguro não funcionará.', 'kadum-opts'),
				'validate' => 'no_html',
				'msg' => 'Você não pode utilizar HTML neste campo. Todas as tags HTML foram removidas.',
				'std' => 'admin@nanpos.com'
			),

			# Página de Criação de Conta
			array(
				'id' => 'config_criar_conta',
				'type' => 'pages_select',
				'title' => __('Criar conta', 'kadum-opts'), 
				'sub_desc' => __('Selecione uma página para a criação de conta.', 'kadum-opts'),
				'desc' => __('</br>Lembre-se de escolher o modelo de página "Criar Conta" para esta página.', 'kadum-opts'),
				'args' => array()
				),	

			# Página de Retorno do Pagseguro
			array(
				'id' => 'config_pagseguro_retorno',
				'type' => 'pages_select',
				'title' => __('Página de Retorno', 'kadum-opts'), 
				'sub_desc' => __('Selecione uma página para o retorno do pagseguro.', 'kadum-opts'),
				'desc' => __('</br>Lembre-se de escolher o modelo de página "Pagamentos" para esta página.', 'kadum-opts'),
				'args' => array()
				),			

		),
	);			
				
	global $NHP_Options;
	$NHP_Options = new NHP_Options($sections, $args);

}