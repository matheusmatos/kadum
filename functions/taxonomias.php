<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 *
 * Aqui é feito o registro das seguintes taxonomias para os Anúncios: Áreas de Atuação e Cidades.
 * Aqui também são adicionadas automaticamente as Áreas de Atuação aos Menus do Wordpress.
 *
**/


/*** GANCHOS ***/

add_action('init', 'kadum_taxonomia_area_de_atuacao', 9);

add_action('init', 'kadum_taxonomia_cidades', 10);

add_action('init', 'kadum_add_tax_menu_wp', 11);



# Registro da taxonomia Área de Atuação para os anuncios
function kadum_taxonomia_area_de_atuacao() {

	$args = array(
    'labels' => array(
      'name'                       => __( 'Áreas de atuação', 'kadumtheme' ),
      'singular_name'              => __( 'Área de atuação', 'kadumtheme' ),
      'search_items'               => __( 'Pesquisar áreas de atuação', 'kadumtheme' ),
      'popular_items'              => __( 'Áreas de atuação mais procuradas', 'kadumtheme' ),
      'all_items'                  => __( 'Todas áreas de atuação', 'kadumtheme' ),
      'parent_item'                => __( 'Áreas de atuação (Mãe)', 'kadumtheme' ),
      'parent_item_colon'          => __( 'Áreas de atuação (Mãe):', 'kadumtheme' ),
      'edit_item'                  => __( 'Editar área de atuação', 'kadumtheme' ),
      'update_item'                => __( 'Atualizar área de atuação', 'kadumtheme' ),
      'add_new_item'               => __( 'Adicionar nova área de atuação', 'kadumtheme' ),
      'new_item_name'              => __( 'Novo nome da área de atuação', 'kadumtheme' ),
      'separate_items_with_commas' => __( 'Separe as áreas de atuação por vírgulas', 'kadumtheme' ),
      'add_or_remove_items'        => __( 'Adicionar ou remover áreas de atuação', 'kadumtheme' ),
      'choose_from_most_used'      => __( 'Escolha entre as áreas de atuação mais utilizadas', 'kadumtheme' ),
      'menu_name'                  => __( 'Áreas de atuação', 'kadumtheme' ),
  	),
	  'public' => true,
	  'show_in_nav_menus' => false,
	  'show_ui' => true,
	  'show_tagcloud' => false,
	  'query_var' => true,
	  'rewrite' => array('slug' => 'area-atuacao'),
	  'hierarchical' => true
	);

	# Registro da taxonomia: $id, $tipo de post, $argumentos
  register_taxonomy('area_atuacao', array( 'anuncios' ), $args );

  flush_rewrite_rules();
};





# Registro da taxonomia Cidades para os anuncios
function kadum_taxonomia_cidades() {
    
	# Array com argumentos que serão passados
	# na função de registro da taxonomia
	$args = array(
        'labels' => array(
	        'name'                       => __( 'Cidades', 'kadumtheme' ),
	        'singular_name'              => __( 'Cidade', 'kadumtheme' ),
	        'search_items'               => __( 'Pesquisar cidades', 'kadumtheme' ),
	        'popular_items'              => __( 'Cidades mais procurados', 'kadumtheme' ),
	        'all_items'                  => __( 'Todas as cidades', 'kadumtheme' ),
	        'parent_item'                => __( 'Cidade (Mãe)', 'kadumtheme' ),
	        'parent_item_colon'          => __( 'Cidade (Mãe):', 'kadumtheme' ),
	        'edit_item'                  => __( 'Editar cidade', 'kadumtheme' ),
	        'update_item'                => __( 'Atualizar cidades', 'kadumtheme' ),
	        'add_new_item'               => __( 'Adicionar nova cidade', 'kadumtheme' ),
	        'new_item_name'              => __( 'Novo nome da cidade', 'kadumtheme' ),
	        'separate_items_with_commas' => __( 'Separe as cidades por vírgulas', 'kadumtheme' ),
	        'add_or_remove_items'        => __( 'Adicionar ou remover cidades', 'kadumtheme' ),
	        'choose_from_most_used'      => __( 'Escolha entre as cidades mais utilizadas', 'kadumtheme' ),
	        'menu_name'                  => __( 'Cidades', 'kadumtheme' ),
	    ),
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'hierarchical' => true,
		'query_var' => true,
		'rewrite' => array(
			'hierarchical' => true,
			'slug' => 'cidade'
		), // this makes hierarchical URL
	  );

    # Registro da taxonomia: $id, $tipo de post, $argumentos
    register_taxonomy('cidade', array('anuncios'), $args );

    flush_rewrite_rules();
};





/**
 * Adiciona as Áreas de Atuação já cadastradas ao menu superior do Kadum.
 *
 */

function kadum_add_tax_menu_wp() {

	global $pagenow;

  $taxonomia  = 'area_atuacao';
	$local_menu = 'navegacao-categorias';

	if( $pagenow == 'nav-menus.php' ) {

    # Obter o menu da barra amarela
  	$menu = wp_get_nav_menus( array( 'location' => $local_menu ) );

    $menu_id = ( count($menu) ) ? $menu[0]->term_id : false;

    # Obter termos da taxonomia
    $termos = get_terms(
      $taxonomia,
      array(
        'hide_empty' => false,
        'parent' => 0,
        'orderby' => 'count',
        'order' => 'DESC'
      )
    );

    # Se não há taxonomias registradas, não tem porque fazer algo.
    if(! count($termos) ) return null;


    # Obter itens que já estão no menu
    $itens_do_menu = wp_get_nav_menu_items( $menu_id, array('post_status' => 'publish,draft'));

    $itens_id = array();

    foreach( $itens_do_menu as $item ) {
      $itens_id[] = $item->object_id;
    }

    # Loop para adicionar novos itens ao menu
    foreach ( $termos as $key => $termo ){

  		if ( !$termo->term_id || $termo->term_id == 0 ) continue;
  		
  		# Caso queira impor um limite de termos no menu
  		# if ( $key == 9) break;

      # Checa se já existe no menu
  		$existe = in_array($termo->term_id, $itens_id);

  		# Se não existe, adicionar
  		if ( $existe == false ) {		
        $parent_id = 0;

        $args = array(
          'menu-item-object-id' => $termo->term_id,
          'menu-item-object' => $taxonomia,
          'menu-item-type' => 'taxonomy',
          'menu-item-status' => 'publish',
          'menu-item-parent-id' => $parent_id,
          'menu-item-attr-title' => $termo->name,
          'menu-item-description' => $termo->description,
          'menu-item-title' => $termo->name,
          'menu-item-target' => '',
          'menu-item-classes' => 'termid-'.$termo->term_id.' parentid-'.$parent_id,
          'menu-item-xfn' => '',
        );

        # Salva e retorna um inteiro (ID no BD) ou WP_Error
        $save = wp_update_nav_menu_item( $menu_id, 0, $args );
  		}
  	}
  }
}