<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 *	Biblioteca de funções para uso em diversas partes do Kadum
 *	
 *	Aqui são criadas as seguintes funções:
 *	
 *  url();                    // Refaz a URL retirando/adicionando paramêtros GET e retorna os valores
 *  debug();                  // Dá um print_r ou var_dump acompanhado de WP_DIE
 *  get_avatar_url();         // Retorna a URL do gravatar
 *  the_breadcrumb();         // Imprime na tela um breadcumb
 *	kadum_subcategorias();    // Obtém as subcategorias de uma determinada taxonomia.
 *	
**/


/**
 *  Essa função refaz a URL atual.
 *  É possível retirar paramêtros do GET, e adicionar paramêtros.
 *  
 *  Retorna um array com dois valores:
 *      - A URL atual modificada
 *      - Um array com os campos e valores do GET.
 *  
 *  É utilizada, por exemplo, na criação do anúncio para tirar o $plano e o $message da URL.
 *  
**/
function url( $del=false, $add=array() ) {

	# REFAZER URL

	$url = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : NULL ;
	$url = explode('?', $url);
	$get = explode('&', $url[1]);

	$parametros = array();
	foreach($get as $key => $value) {
		$param = explode('=', $value);
		$parametros[$param[0]] = $param[1];
	};

	$valores = $parametros;

	if($del){
		$del = explode(',', $del);
		foreach($del as $key) if(isset($parametros[$key])) unset($parametros[$key]);
	}

	foreach($add as $key => $value) $parametros[$key] = $value;

	$url = $url[0] . '?';
	foreach($parametros as $key => $value) $url .= '&' . $key . '=' . $value;
	$url = str_replace('?&', '?', $url);

	$return = array('url' => $url, 'get' => $valores);

	return $return;

};



/**
 * $post_id - The ID of the post you'd like to change.
 * $status -  The post status publish|pending|draft|private|static|object|attachment|inherit|future|trash.
**/
function esc_file_upload($string){
   // pegando a extensao do arquivo
   $partes      = explode(".", $string);
   $extensao    = $partes[count($partes)-1];
   // somente o nome do arquivo
   $nome            = preg_replace('/\.[^.]*$/', '', $string);  
   // removendo simbolos, acentos etc
   $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýýþÿŔŕ?';
   $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuuyybyRr-';
   $nome = strtr($nome, utf8_decode($a), $b);
   $nome = str_replace(".","-",$nome);
   $nome = preg_replace( "/[^0-9a-zA-Z\.]+/",'-',$nome);
   return utf8_decode(strtolower($nome.".".$extensao));
}


/**
 * $post_id - The ID of the post you'd like to change.
 * $status -  The post status publish|pending|draft|private|static|object|attachment|inherit|future|trash.
**/
function pr( $debug , $title = '', $var_dump = false ){
    print_r($title);
    print_r('<pre>');
    if($var_dump) print_r(var_dump($debug));
    else print_r($debug);
    print_r('</pre>');
    wp_die();
}



/**
 * $post_id - The ID of the post you'd like to change.
 * $status -  The post status publish|pending|draft|private|static|object|attachment|inherit|future|trash.
**/
function change_post_status($post_id,$status){
    $current_post = get_post( $post_id, 'ARRAY_A' );
    $current_post['post_status'] = $status;
    wp_update_post($current_post);
}



/**
 *  Essa função obtém a URL do Gravatar
 *  
**/
function get_avatar_url($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    return $matches[1];
};





/**
 *  Essa função imprime na tela um breadcumb
 *  
**/
function the_breadcrumb() {
    echo '<ul class="breadcrumb">';
    echo '<li><a href="'. home_url('/') .'"><i class="icon-home icon-blue"></i></a> <span class="divider">&rsaquo;</span></li>';
    
    if (is_home()){
    	echo '<li class="active">Anúncios</li>';
	}elseif( is_category() || is_single() || is_page() ){
        
        if (is_category() || is_single()) {
            the_category('title_li=');
            if (is_single()) {
                echo " Â» ";
                the_title();
            }
        } elseif (is_page()) {
            echo '<li class="active"><a href="">' . the_title() . '</a></li>';
        }
    }

    echo '</ul>';
};





/**
 *  Essa função obtém as subcategorias de uma determinada taxonomia.
 *  
**/
function kadum_subcategorias( $taxonomy = 'area_atuacao' ){


    $current_tax = get_query_var('area_atuacao');
    $termo_pai = get_term_by('slug', $current_tax, $taxonomy);
    $filhos = get_term_children($termo_pai->term_id, $taxonomy);

    if( count($filhos) != 0 ){

        echo "<ul>";
        
        foreach($filhos as $filho){
            $termo = get_term_by('id', $filho, $taxonomy);
            echo '<li><a href="' . get_term_link( $termo->name, $taxonomy ) . '">' . $termo->name . '</a></li>';
        }

        echo '</ul>';
    
    }else  print_r('Não há subcategorias');

};