<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Antes
**/

header('Content-Type: text/html; charset=ISO-8859-1');

# Array contendo opções do Kadum
$opcoes = get_option('opcoes-kadum');

# Inicia o processo
if( isset($_POST['notificationType']) && ($_POST['notificationType'] == 'transaction') ){

  # Credenciais
  $email = $opcoes['config_pagseguro_email'];
  $token = $opcoes['config_pagseguro_token'];

  /**
   * Iniciando a verificação no PAGSEGURO através de CURL
   * --------------------------------------------------------------
  **/
  $url = 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/' . $_POST['notificationCode'] . '?email=' . $email . '&token=' . $token;

  $curl = curl_init($url);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  $transaction = curl_exec($curl);
  curl_close($curl);


  /**
   * Retorno do PAGSEGURO, atualizar no POST ou mostrar erros
   * --------------------------------------------------------------
  **/
  if( $transaction == 'Unauthorized' ){
      
    # Define como true para loop na página
    $Pagamento_errors = true;

    exit;
  }

  $transaction = simplexml_load_string($transaction);

  # Code
  $post_id = $transaction->reference;

  /**
   * PAGSEGURO STATUS DE TRANSAÇÃO DISPONÍVEIS:
   *  1 = Aguardando pagamento 
   *  2 = Em análise
   *  3 = Paga 
   *  4 = Disponível
   *  5 = Em disputa
   *  6 = Devolvida
   *  7 = Cancelada
   * 
   * STATUS DO KADUM:
   *  0 = Aguardando pagamento
   *  1 = Cancelado
   *  2 = Pagamento confirmado
  **/
  switch( $transaction->status ){
    
    # Aguardando pagamento...
    case 1:
    case 2:
      
      $novo_status = 0;
      update_post_meta($post_id, '_kds_status', $novo_status);

      break;
    
    # Pagamento confirmado...
    case 3:
    case 4:
      
      $novo_status = 2;
      update_post_meta($post_id, '_kds_status', $novo_status);



      break;
    
    # Pagamento cancelado ou com algum problema...
    case 5:
    case 6:
    case 7:
      
      $novo_status = 1;
      update_post_meta($post_id, '_kds_status', $novo_status);

      global $wpdb;

      $wpdb->update(
          'wp_posts',
          array('post_status' => 'publish'),
          array( 'ID' => $post_id ),
          array( '%s' ),
          array( '%d' )
      );

    break;
  }
}


/**
 * Não há POST, ou não é do PagSeguro
 * --------------------------------------------------------------
**/
else{


}