<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * ----------------------------------------------------------------------------
 * Aqui é feito o registro dos campos personalizados(metabox) para os Anúncios
 * 
 * É criado apenas um MetaBox, que contem todos campos personalizados necessários:
 *   - Seleção do plano de ofertas;
 *   - Título; (personalizado, não o do Wordpress)
 *   - Telefone;
 *   - Descrição;
 *   - Foto;
 *   - Endereço;
 *   - Mapa no Google Mapas.
**/


# Pega a URL atual e retorna os valores passados via $_GET
# A função url() está no /functions/kadum.lib.php, detalhes na documentação.
$parametros = url('message,plano');
$url = $parametros['url'];
$get = $parametros['get'];


# Array contendo opções do Kadum
$opcoes = get_option('opcoes-kadum');


# Criação da variável $plano, que mostra o plano ativo no momento.
# Se existe um plano no GET, use ele, senão...
# Se existe um plano salvo, use ele, senão...
# Use um valor padrão
$plano_salvo = $mb->get_the_value('plano');

if( isset($get['plano']) ) $plano = $get['plano'];
elseif( isset($plano_salvo) ) $plano = $plano_salvo;
else $plano = 1;


/**
 * Os campos a seguir não são salvos com a classe WP_Alchemy, pois comprometeria a segunrança
 * visto que, um usuário mal-intencionado, poderia criar um hidden input com o name, alterando o valor.
 * Por isso, estes campos são salvos separadamente com o prefixo _kds_ (kadum_secure)
**/

# Salva o prazo que expirará
$expira_em = get_post_meta($post->ID, '_kds_expira', true);

if( isset($expira_em) && (!count($expira_em) == 0) ){
    $prazo = $opcoes['plano'.$plano.'_periodo'];
    $expira = date('Y-m-d', strtotime($post->post_date . ' +'.$prazo.'days') );

    update_post_meta($post->ID, '_kds_expira', $expira);
}


# Definindo um valor padrão para a notificação.
# Este campo mostra se já foi feita a notificação ao anunciante.
$notificacao = get_post_meta($post->ID, '_kds_notificacao', true);
if( isset($notificacao) && (!count($notificacao) == 0) ) update_post_meta($post->ID, '_kds_notificacao', 0);


# Tenta obter o status de pagamento atual
# Se já existe, use ele, senão, use um padrão (aguardando pagamento)
$status_atual = (int)get_post_meta($post->ID, '_kds_status', true);
if( isset($status_atual) && (is_int($status_atual)) && ($status_atual <= 2 ) ){
    $status = $status_atual;
}else{
    $status = 0;
    update_post_meta($post->ID, '_kds_status', $status);
}


# Tenta obter as coordenadas geográficas atuais
# Se já existe, use elas, senão, use um padrão (nas configurações)
$coordenadas_atuais = $mb->get_the_value('coordenadas');
if( isset($coordenadas_atuais) ) $coordenadas = $coordenadas_atuais;
else $coordenadas = $opcoes['config_coordenadas'];

/**
 * PAGAMENTO - Integração com o Pagseguro a partir daqui
 *
 * Se o usuário escolheu um plano pago, são salvas algumas informações da transação, com status = 0 (aguardando pagamento)
 * Logo após, se o status não for pago, é criado um novo payment request.
 * 
 *
**/
require_once(metabox.ds.'pagseguro'.ds.'PagSeguroLibrary.php');

if( $opcoes['plano'.$plano.'_preco'] != 0 ) {

    # Informações do usuário logado.
    global $current_user;

    # Status de pagamento
    $nomes_status = array(
        0 => 'Aguardando pagamento',
        1 => 'Cancelado',
        2 => 'Pagamento confirmado',
    );

    # Se status é 2, o pagamento está confirmado.
    if( $status == 2 ) {

        // code

    }else{

        try{

            # Nova requisição de pagamento
            $paymentRequest = new PagSeguroPaymentRequest();

            # Adiciona item: Plano do Kadum
            $produto = 'plano_' . $plano;
            $nome    = $opcoes['plano'.$plano.'_nome'];
            $qtd     = 1;
            $preco   = number_format($opcoes['plano'.$plano.'_preco'], 2, '.', '');

            $paymentRequest->addItem($produto, $nome, $qtd, $preco);

            # Adiciona o cliente
            $paymentRequest->setSender(
              $current_user->data->display_name,
              $current_user->data->user_email
            );

            # Adiciona o endereço de entrega (endereço do kadum)
            $paymentRequest->setShippingAddress(
                '01452002',
                'Av. Brig. Faria Lima',
                '1384',
                'apto. 114',
                'Jardim Paulistano',
                'São Paulo',
                'SP',
                'BRA'
            );

            # Configura a moeda para Real do Brasil
            $paymentRequest->setCurrency('BRL');

            # Informação de frete
            $paymentRequest->setShippingType(3);

            # Código único
            $paymentRequest->setReference($post->ID);

            # Informando as credenciais
            $credentials = new PagSeguroAccountCredentials(
              $opcoes['config_pagseguro_email'],
              $opcoes['config_pagseguro_token']
            );
      
            # Fazendo a requisição a API do PagSeguro pra obter a URL de pagamento
            $url_pagamento = $paymentRequest->register($credentials);
        
        
        }catch( Exception $e ){
            
            $message = (string)$e->getMessage();
            $PagSeguro_Error = array();

            if( $message == "[HTTP 503] - UNDEFINED " ){
                $PagSeguro_Error['titulo'] = 'Não foi possível se conectar ao PagSeguro';
                $PagSeguro_Error['descricao'] = 'O PagSeguro está passando por problemas e não está disponível no momento.</br>Tente novamente mais tarde.';
            }

            elseif( $message == 'Outra CODE ERROR QUE NÃO ESSE' ){
                $PagSeguro_Error['titulo'] = 'Título para o CODE ERROR';
                $PagSeguro_Error['descricao'] = 'Descrição para o CODE ERROR.';
            }

            else{
                $PagSeguro_Error['titulo'] = 'Não foi possível registrar seu pedido';
                $PagSeguro_Error['descricao'] = 'Houve algum problema e não conseguimos registrar seu pedido. Tente novamente mais tarde.';
            }

        }
    }
}

# Continuação..

# Inclui o template
include_once(metabox . ds . 'template.php');