/*
 * jQuery stringToSlug plug-in 1.2.1
 *
 * @link        https://github.com/leocaseiro/jQuery-Plugin-stringToSlug
 * @link        http://leocaseiro.com.br/jquery-plugin-string-to-slug/
 * @license     http://www.opensource.org/licenses/mit-license.php
 * @license     http://www.gnu.org/licenses/gpl.html
 * @copyright   Copyright (c) 2009 Leo Caseiro
 * 
 * Based on Edson Hilios (http://www.edsonhilios.com.br/ Algoritm
 */
jQuery.fn.stringToSlug=function(options){var defaults={setEvents:'keyup keydown blur',getPut:'#permalink',space:'-',prefix:'',suffix:'',replace:''};var opts=jQuery.extend(defaults,options);jQuery(this).bind(defaults.setEvents,function(){var text=jQuery(this).val();text=defaults.prefix+text+defaults.suffix;text=text.replace(defaults.replace,"");text=jQuery.trim(text.toString());var chars=[];for(var i=0;i<32;i++){chars.push('')}chars.push(defaults.space);chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push("");chars.push(defaults.space);chars.push(defaults.space);chars.push('');chars.push('');chars.push(defaults.space);chars.push(defaults.space);chars.push(defaults.space);chars.push(defaults.space);chars.push('0');chars.push('1');chars.push('2');chars.push('3');chars.push('4');chars.push('5');chars.push('6');chars.push('7');chars.push('8');chars.push('9');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('A');chars.push('B');chars.push('C');chars.push('D');chars.push('E');chars.push('F');chars.push('G');chars.push('H');chars.push('I');chars.push('J');chars.push('K');chars.push('L');chars.push('M');chars.push('N');chars.push('O');chars.push('P');chars.push('Q');chars.push('R');chars.push('S');chars.push('T');chars.push('U');chars.push('V');chars.push('W');chars.push('X');chars.push('Y');chars.push('Z');chars.push(defaults.space);chars.push(defaults.space);chars.push(defaults.space);chars.push('');chars.push(defaults.space);chars.push('');chars.push('a');chars.push('b');chars.push('c');chars.push('d');chars.push('e');chars.push('f');chars.push('g');chars.push('h');chars.push('i');chars.push('j');chars.push('k');chars.push('l');chars.push('m');chars.push('n');chars.push('o');chars.push('p');chars.push('q');chars.push('r');chars.push('s');chars.push('t');chars.push('u');chars.push('v');chars.push('w');chars.push('x');chars.push('y');chars.push('z');chars.push(defaults.space);chars.push('');chars.push(defaults.space);chars.push('');chars.push('');chars.push('C');chars.push('A');chars.push('');chars.push('f');chars.push('');chars.push('');chars.push('T');chars.push('t');chars.push('');chars.push('');chars.push('S');chars.push('');chars.push('CE');chars.push('A');chars.push('Z');chars.push('A');chars.push('A');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push(defaults.space);chars.push(defaults.space);chars.push('');chars.push('TM');chars.push('s');chars.push('');chars.push('ae');chars.push('A');chars.push('z');chars.push('Y');chars.push('');chars.push('');chars.push('c');chars.push('L');chars.push('o');chars.push('Y');chars.push('');chars.push('S');chars.push('');chars.push('c');chars.push('a');chars.push('');chars.push('');chars.push('');chars.push('r');chars.push(defaults.space);chars.push('o');chars.push('');chars.push('2');chars.push('3');chars.push('');chars.push('u');chars.push('p');chars.push('');chars.push('');chars.push('1');chars.push('o');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('A');chars.push('A');chars.push('A');chars.push('A');chars.push('A');chars.push('A');chars.push('AE');chars.push('C');chars.push('E');chars.push('E');chars.push('E');chars.push('E');chars.push('I');chars.push('I');chars.push('I');chars.push('I');chars.push('D');chars.push('N');chars.push('O');chars.push('O');chars.push('O');chars.push('O');chars.push('O');chars.push('x');chars.push('O');chars.push('U');chars.push('U');chars.push('U');chars.push('U');chars.push('Y');chars.push('D');chars.push('B');chars.push('a');chars.push('a');chars.push('a');chars.push('a');chars.push('a');chars.push('a');chars.push('ae');chars.push('c');chars.push('e');chars.push('e');chars.push('e');chars.push('e');chars.push('i');chars.push('i');chars.push('i');chars.push('i');chars.push('o');chars.push('n');chars.push('o');chars.push('o');chars.push('o');chars.push('o');chars.push('o');chars.push('');chars.push('o');chars.push('u');chars.push('u');chars.push('u');chars.push('u');chars.push('y');chars.push('');chars.push('y');chars.push('z');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('C');chars.push('c');chars.push('D');chars.push('d');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('E');chars.push('e');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('N');chars.push('n');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('R');chars.push('r');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('S');chars.push('s');chars.push('');chars.push('');chars.push('T');chars.push('t');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('U');chars.push('u');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('');chars.push('Z');chars.push('z');for(var i=256;i<100;i++){chars.push('')}var stringToSlug=new String();for(var i=0;i<text.length;i++){stringToSlug+=chars[text.charCodeAt(i)]}stringToSlug=stringToSlug.replace(new RegExp('\\'+defaults.space+'{2,}','gmi'),defaults.space);stringToSlug=stringToSlug.replace(new RegExp('(^'+defaults.space+')|('+defaults.space+'$)','gmi'),'');stringToSlug=stringToSlug.toLowerCase();jQuery(defaults.getPut).val(stringToSlug);jQuery(defaults.getPut).html(stringToSlug);return this});return this}


/*
 * Kadum Capitalize
 */
;jQuery.capitalize = function(string){ return string.charAt(0).toUpperCase() + string.slice(1); }

;jQuery.fn.capitalize = function(){val = $(this).val(); cap = $.capitalize(val); $(this).val(cap); return this; };


/**
 * Kadum Google Maps Initialize
 * - Pega o endereço
 * - Geocodifica
 * - Exibe o mapa
**/

jQuery.NovoMapa = function( endereco, callbackError ){
  
  
  coordenadas_padrao = jQuery('#coordenadas-padrao').val();
  coordenadas_atuais = jQuery('#coordenadas-atuais').val();

  /* @Método: __constructor
   * -----------------------------------
  **/
  if( endereco ){

    // Obtém as coordenadas através de Geocoder
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( {'address': endereco }, function(results, status) {
      
      if (status == google.maps.GeocoderStatus.OK) {
        latlng = results[0].geometry.location;
        
        // Exibe o mapa mostrando o balão de confirmação, pois não está confirmado!
        jQuery.gerarMapa(latlng);

      }else callbackError(status);

    });
  
  }

  else if( coordenadas_padrao != coordenadas_atuais ) {
    
    // Obtém as coordenadas
    coordenadas = coordenadas_atuais.split(',');
    latlng = new google.maps.LatLng(coordenadas[0], coordenadas[1]);
    
    // Cria o mapa sem mostrar o balão, pois já está confirmado o local.
    jQuery.gerarMapa(latlng, true);

  }else if( navigator.geolocation ) {
    
    navigator.geolocation.getCurrentPosition( function(position){
      latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      
      // Exibe o mapa mostrando o balão de confirmação, pois não está confirmado!
      jQuery.gerarMapa(latlng);
    });

  }

  else{

    // Obtém as coordenadas
    coordenadas = coordenadas_atuais.split(',');
    latlng = new google.maps.LatLng(coordenadas[0], coordenadas[1]);
    
    // Cria o mapa sem mostrar o balão, pois já está confirmado o local.
    jQuery.gerarMapa(latlng, true);
  }

};

jQuery.gerarMapa = function( latlng, noCloudInfo ){

  /* Esta parte se assemelha com uma classe javascript.
   * ---------------------------------------------------------
   *   1 - Para 'instanciar' chame o $.NovoMapa que por sua vez indentificará
   *       uma latitude/longitude através de endereço, geolocation, ou coordenadas padrão.
   * 
   *   @method __constructor
   *   @description - Não é bem um método, fica no final. Cria um novo mapa e chama um método
   *                  que por usa vez vai chamando outros métodos e criando um fluxo.
   * 
   *   @method - Marcador()
   *   @description - Cria um novo marcador recebendo como parametro latitude e longitude
   *                  e define um callback para o evento de clique no marcador recém-criado.
   * 
   *   @method - CloudInfo()
   *   @description - Cria um balão de confirmação para um marcador definido. Pode ser chamado
   *                  logo após ter criado o marcador, ou através do evento 'click'
   * 
   *   @method - OcultarInfos()
   *   @description - Remove todos balões de confirmação abertos.
   * 
   *   @method - setarLocal()
   *   @description - Centraliza o mapa em um novo local. É chamado no load do mapa ou no evento 'click' no mapa.
   *                  Ele mostra o balão apenas em alguns casos, em função do parâmetro noCloudInfo
   *
  **/

  // Propriedades
  marcadores = new Array();
  infowindowAtual = 0;
  latlngAtual = null;
  mostrarCloud = ( noCloudInfo == true ) ? false : true;


  /* Mostrando um marcador...
   * -----------------------------------
  **/
  var Marcador = function( latlng ){

    for( key in marcadores ){
      marcadores[key].setMap(null);
    }

    marcadorLocal = new google.maps.Marker({
      position: latlng,
      map: mapa,
      title: "Você está aqui?",
    });

    marcadores.pop();
    marcadores.push(marcadorLocal);

    latlngAtual = latlng.toString().replace(', ', ',').replace('(','').replace(')','');

    // Evento de clique no marcador.
    google.maps.event.addListener(marcadorLocal, 'click', function() { return CloudInfo(marcadorLocal) });

    return marcadorLocal;
  }


  /* Mostrando um balão de confirmação...
   * --------------------------------------
  **/
  var CloudInfo = function( marcador ){

    // Se já existe um Window, feche-o.
    if( infowindowAtual ) infowindowAtual.close();

    // Crie o window atual...
    infowindow = new google.maps.InfoWindow({
      
      // Content com o botão "Não" (o evento click precisa ser revisto para fechar o balão)
      //content: "<div id='kd_infoBox'><h2>Você está aqui?</h2><div class='buttons'><a class='button' id='kd_infobox_sim'>Sim</a> <a class='button' id='kd_infobox_nao'>Não</a></div></div>"
      
      content: "<div id='kd_infoBox'><h2>Você está aqui?</h2><div class='buttons'><a class='button' id='kd_infobox_sim'>Sim</a></div></div>"
    });

    // Salve na variável global, e mostre-o
    infowindowAtual = infowindow;
    infowindow.open(mapa, marcador);

    // Evento de clique nos botões do marcador.
    // Setados após o dom do infowindow estar disponível
    // Referência de eventos do GMaps: https://developers.google.com/maps/documentation/javascript/reference
    google.maps.event.addListener(infowindow, 'domready', function() {
      
      // Este callback realiza ações após o usuário clicar em "Sim"
      google.maps.event.addDomListener(document.getElementById('kd_infobox_sim'), 'click', function(){

        jQuery('#coordenadas-atuais').val(latlngAtual);
        jQuery('#info_mostrar_no_mapa .esq p').mapaMsg('Pronto! Seu local está escolhido no mapa!');
        jQuery('#modificar_local_mapa a.button:visible').click();
        infowindowAtual.setContent("<div id='kd_infoBox'><h2>Obrigado!</h2><p>Seu local foi escolhido no mapa. Feche esta janela.</p></div>");
        // OcultarInfos();
        
      });

      // Caso o botão "Não" passe a ser exibido, este callback trata de fechar o Box
      // ATENÇÃO: apresentou um bug com o marcador pulando ao utilizar apenas o método close();
      // google.maps.event.addDomListener(document.getElementById('kd_infobox_nao'), 'click', function(){
      //   OcultarInfos();
      // });

    });

  }


  /* Ocultando todas as infos
   * -----------------------------------
  **/
  var OcultarInfos = function(){
    
    infowindowAtual.close();

  }


  /* Setando um novo local...
   * -----------------------------------
  **/  
  var setarLocal = function( latlng ){
    marcador = Marcador( latlng );
    
    // Se mostrarCloud estiver como true, mostre, se não, defina como true para voltar a mostrar
    if( mostrarCloud == true ) CloudInfo(marcador);
    else mostrarCloud = true;
  }

  
  /* @start __constructor()
  **/

  /* Gerando o Mapa...
   * -----------------------------------
  **/
  NivelZoom = (mostrarCloud == true) ? 15 : 18;

  var mapa = new google.maps.Map(document.getElementById("kadum_frame_mapa"), {
    zoom: NivelZoom,
    center: latlng,
    mapTypeControl: false,
    navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  // Setando o local inicial
  setarLocal( latlng );

  // Evento de clique no mapa.
  google.maps.event.addListener(mapa, 'click', function(event) {
    jQuery('#modificar_local_mapa a.button').click();
    setarLocal(event.latLng)
  });

  /**
   * @end __constructor()
  **/

};


jQuery.fn.mapaMsg = function( msg ){
  
  jQuery(this).fadeOut('slow', function(){
    jQuery(this).css('background','#feffac')
    .html(msg)
    .fadeIn(function(){
      eu = jQuery(this);
      window.clearInterval();
      window.setTimeout(function(){
        eu.css('background','transparent');
      }, 1500);
    });
  });

  return this;
};