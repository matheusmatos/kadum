/**
 * Kadum.com.br
**/

(function($) {

  jQuery(document).ready(function(){

    // Adiciona classe kadum-js a div que engloba os campos personalizados
    $('#kadum_campos_personalizados').addClass('kadum-js');

    
    /**
     * Formulário de criação de anúncio
     * ------------------------------------------------------------------
    **/

    // Gera e mostra o slug igual ao do wordpress de acordo com o título
    $('#string_titulo').stringToSlug({getPut: '#seu-slug span'});
    $('#string_titulo').keyup(function(){
      $('#seu-slug-input').html( $('#seu-slug span').html() );
    });


    /**
     * Validação dos inputs de texto
     * Funciona em qualquer input text
    **/
    $('.kadum_meta .label input:text').each(function(){
      var input    = $(this);
      var maximo   = input.attr('maxlength');
      input.keyup(function(){
        var tamanho  = input.val().length;
        var span     = input.next('span');
        var restante = parseInt(maximo - tamanho);

        if(restante==0) span.html('0 caracteres restantes. Esse é o máximo.');
        else if(restante==1) span.html('1 caractere restante.');
        else if(restante>1) span.html(restante + ' caracteres restantes.');
      });

    });

    /**
     * Validação do Textarea
     * Funciona em qualquer textarea, basta definir o atributo maximo.
    **/
    $('textarea[maximo]').each(function(){
      var text    = $(this);
      var maximo  = parseInt($(this).attr('maximo'));
      var tamanho = $(this).val().length;
      var minimo  = 60;
      var exemplo = 'Por exemplo: O kadum é um incrível portal onde você pode anunciar o seu comércio ou o seu serviço.';
      
      var span = $(this).next('span');
      
      if(tamanho < minimo){
        span.html('Use no mínimo '+ minimo +' caracteres e no máximo <b>'+ maximo +'</b>.');
      }else{
        span.html('<b></b> caracteres restantes.');
        spanb = span.children('b');
        spanb.html(maximo - tamanho);
      };
      
      text.keyup(function(){
        tamanho = text.val().length;
        
        if(tamanho < minimo){
          span.css('color','Crimson').html('Atenção! Use no mínimo '+ minimo +' caracteres. Você já usou <b>'+ tamanho +'</b> de '+ maximo +'.');
          span.attr('invalido','Use no mínimo 50 caracteres.');
        }else if(tamanho == maximo){
          span.css('color','Green').html('Pronto! Esse é o máximo.');
        }else if(tamanho > maximo){
          span.css('color','Crimson').html('Ops! Use no máximo '+ maximo +' caracteres. Você já usou <b>'+ tamanho +'</b>!');
        }else if(tamanho >= (maximo - 30)){
          span.css('color','Crimson');
          resta = maximo - tamanho;
          if(resta == 1){
            span.html('Atenção! Resta apenas <b>'+ resta +'</b> caractere. Que tal um ponto final?');
          }else{
            span.html('Atenção! Restam apenas <b>'+ resta +'</b> caracteres.');
          }
        }else if(tamanho < maximo){
          span.css('color','#999999').html('<b>'+ (maximo - tamanho) +'</b> caracteres restantes.');
        };
      });

      text.blur(function(){
        if(tamanho == 0){
          span.css('color','#999999').html('Ué! Cadê a descrição? Use no mínimo 60 caracteres e no máximo ' + maximo + '.');
        }
      });

      text.focus(function(){
        if(text.val() == exemplo){
          text.val('');
          span.css('color','green').html('É simples! Vamos lá, comece!');
        }
      });
    });

    // Validação Javascript do formulário
    // $('#post').submit(function(){});

    
    
    /**
     * FOTOS E UPLOADS DE IMAGENS.
    **/

    // Habilita o botão de upload após o DOM ser carregado
    $('#botao_upload_disabled').hide();
    $('#botao_upload').css('display','inline-block');


    /**
     * Consultar o status
     * ------------------------------------------------------------------
    **/
    $('#consultar-status').click(function(){
        
      eu = $(this);
      eu.html('Atualizando...');
      id = $(this).attr('data-id');

      jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: {
          'action': 'kadum_consultar_status',
          'id': id
        },
        dataType: 'html',

        success: function(status) {
          
          $('#kadum_pagamento h2 small').fadeOut('slow', function(){
            $(this).html("Status: " + status).fadeIn();
            eu.html('Atualizar status');
          })

        },

        error: function( msg ){
          
          $('#kadum_pagamento h2 small').fadeOut('slow', function(){
            $(this).html("Não foi possível atualizar o status de pagamento.").fadeIn();
            eu.html('Atualizar status');
          })

        }

      });

      return false;
    });


    /**
     * Função para atualizar o formulário de Upload de Foto
     * ------------------------------------------------------------------
    **/
    jQuery.atualizarBotaoUpload = function(){

      var upload    = $('#fotos_upload');
      var total     = $('#fotos_galeria .foto-miniatura').length;
      var maximo    = $('#fotos_upload meta[name="kd_nfotos"]').attr('value');
      var permitido = maximo - total;

      $('#fotos_upload #fotos_maximo').html(maximo);
      $('#fotos_upload #fotos_permitido').html(permitido);

      if ( total < maximo ) upload.show();
      
      else if( total == maximo ) upload.hide();

    };

    /**
     * Adicionar uma imagem!
     * ------------------------------------------------------------------
    **/
    var upload    = $('#fotos_upload');
    var status    = $('#kadum_fotos .msgs.status');
    var statusmsg = $('#kadum_fotos .msgs.status p');
    var statusbtn = $('#kadum_fotos .msgs.status a');    
    var nonce     = upload.children('meta[name="nonce_upload"]').attr('value');
    var id        = upload.children('meta[name="post_id"]').attr('value');        

    $("#botao_upload").ajaxUpload({
      url : ajaxurl,
      name: "foto_kadum",
      data: {
        'action': 'kadum_add_attachments',
        '_wpnonce': nonce,
        '_post_id': id,
      },

      onSubmit: function() {
        statusbtn.hide();
        status.removeClass('fracasso').show('fast');
        statusmsg.html('Aguarde, fazendo upload...');
      },

      onComplete: function( json ) {
        
        status.removeClass('fracasso').show();
        statusmsg.html('Upload feito com sucesso!');
        statusbtn.show('slow');
        
        var html = JSON.parse(json);
        $('#fotos_galeria').prepend(html);

        $.atualizarBotaoUpload();
      }

    });

    
    /**
     * Remover uma imagem!
     * ------------------------------------------------------------------
    **/
    $('#kadum_fotos .msgs.status a.button.ok').click(function(){
      $(this).parents('.msgs.status').hide('fast');
      return false;
    });

    $('#fotos_galeria').on('click', '.foto-miniatura', function(){
      
      var foto    = $(this);
      var confirm = $('#kadum_fotos .msgs.confirm');

      // Mostra a confirmação
      confirm.show('fast');
      $('html, body').animate({
          scrollTop: confirm.offset().top - 20
      }, 800);      
      
      // Se clicou no SIM
      $('#kadum_fotos .msgs.confirm a#confirmar').click(function(){

        // Oculta a confirmação
        confirm.hide();

        var status    = $('#kadum_fotos .msgs.status');
        var statusmsg = $('#kadum_fotos .msgs.status p');
        var statusbtn = $('#kadum_fotos .msgs.status a');
        var nonce     = foto.children('meta[name="kd_foto_nonce"]').attr('value');
        var id        = foto.children('meta[name="kd_foto_id"]').attr('value');

        statusbtn.hide();
        status.removeClass('fracasso').show('fast');
        statusmsg.html('Aguarde, removendo...');

        jQuery.ajax({
          url: ajaxurl,
          type: 'POST',
          data: {
            'action': 'kadum_remove_attachments',
            '_wpnonce': nonce,
            '_attachment_id': id,
          },
          dataType: 'html',

          success: function(response) {
            
            foto.hide('slow').remove();
            statusmsg.html('Miniatura removida com sucesso!');
            statusbtn.show('slow');
            $.atualizarBotaoUpload();

          },

          error: function( msg ){
            
            status.addClass('fracasso');
            statusmsg.html('Ocorreu um erro interno no Kadum. Tente novamente em instantes.');
            statusbtn.show('slow');

          }
        });

      });

      // Se clicou no NÃO
      $('#kadum_fotos .msgs.confirm a#cancelar').click(function(){
        $('#kadum_fotos .msgs.confirm').hide('fast');
      });

      return false;
    });


    /**
     * MAPA
    **/
    
    // Inicializa a API do Google Maps
    $('#mostrar_mapa').click(function(){
      eu = $(this);
      endereco = $('#kadum_endereco .label input:text').val();

      $('html, body').animate({ scrollTop: eu.offset().top - 40 }, 800);

      $('#info_mostrar_no_mapa .esq p').mapaMsg('Caso o seu endereço não tenha sido detectado corretamente, clique em um local no mapa.');

      $.NovoMapa(endereco, function(status){

        if( status = 'ZERO_RESULTS' ){
          $('#info_mostrar_no_mapa .esq p').mapaMsg('Não foi possível localizar seu endereço no mapa. Localize seu endereço manualmente e clique para marcá-lo.');
        }else if( status = 'OUTRO_CODE' ){
          $('#info_mostrar_no_mapa .esq p').mapaMsg('Outra mensagem de erro');
        }else{
          $('#info_mostrar_no_mapa .esq p').mapaMsg('Ocorreu um problema com o Google Maps. Tente novamente mais tarde.');
        }

      });

    });

    $.NovoMapa();

    $('#modificar_local_mapa a.button').click(function(){
      $('#modificar_local_mapa').remove();
      info = $('#info_mostrar_no_mapa');
      info.removeClass('oculto');
      $('html, body').animate({ scrollTop: info.offset().top - 20 }, 800);
    });
    
    
    /**
     * Submit do post de criação do anúncio!
     * ------------------------------------------------------------------
    **/
    $('#msgs_erros a.fechar').click(function(){
      $(this).parents('#msgs_erros').fadeOut();
    });

    $('#pusblish').submit();

    $('form#post').submit(function(){

      form = $(this);

      // Se já foi validado abaixo e está no reenvio do form...
      if( form.data('formulario_valido') ){

        // Salva o nome original do 
        if( original == "Atualizar" ) $('#publish').fadeOut('fast').val('Atualizando...').fadeIn('fast');
        else if( original == "Publicar" ) $('#publish').fadeOut('fast').val('Publicando...').fadeIn('fast');
        else $('#publish').fadeOut('fast').val('Enviando...').fadeIn('fast');
        return true;
      
      }else{
        original = $('#publish').val();
        form.data('publish_original', original);
      }

      $('#publish').val('Validando...').attr('disabled','disabled').addClass('button-primary-disabled').css('cursor','wait');

      var erros = new Array();

      var id                 = $('#kadum_metabox').attr('data-id'); // Pegando o ID
      var plano              = $('input[name="kadum_campos[plano]"]').val(); // Pegando o plano
      var titulo             = $('#kadum_titulo .label #string_titulo').val() // Pegando o Título
      var telefone           = $('#kadum_telefone .label input:text').val(); // Pegando o Telefone
      var descricao          = $('#kadum_descricao .label textarea').val(); // Pegando a Descrição
      var descricao_max      = $('#kadum_descricao .label textarea').attr('maximo'); // Pegando a máximo da descrição
      var endereco           = $('#kadum_endereco .label input:text').val(); // Endereço
      var fotos              = $('#fotos_galeria .foto-miniatura').length; // Colocou alguma foto?
      var coordenadas_atuais = $('#coordenadas-atuais').val(); // Mapa
      var coordenadas_padrao = $('#coordenadas-padrao').val(); // Mapa
      var area_atuacao       = ( $('#area_atuacaochecklist input:checked')[0] ) ? true : false; // Escolheu área de atuação?
      var cidade             = ( $('#cidadechecklist input:checked')[0] ) ? true : false; // Escolheu área de atuação?



      /* Aqui é feita um validação mais simples, para evitar requisições ao servidor
      */
      if( titulo == "" || titulo.length < 3 ) erros.push('Escreva um título com pelo menos 3 caracteres.');
      if( telefone == "" ) erros.push('Escreva um telefone no formato: (00) 4444.9999');
      if( descricao == "" || descricao.length < 60 ) erros.push('Escreva uma descrição com mais de 60 caracteres.');
      else if( descricao.length > descricao_max ) erros.push('Escreva uma descrição com no máximo ' + descricao_max + ' caracteres.');
      if( endereco == "" || (endereco == true && endereco.length < 5) ) erros.push('Escreva um endereço completo.');
      if( fotos == 0 ) erros.push('Coloque pelo menos uma foto no seu anúncio');
      else if( plano == 1 && fotos != 1 ) erros.push('Neste plano você só pode colocar uma foto. Deixe apenas uma.');
      else if( plano == 2 && fotos > 5  ) erros.push('Neste plano você só pode colocar no máximo 5 fotos. Você já tem ' + fotos + ' fotos.');
      else if( plano == 3 && fotos > 10  ) erros.push('Neste plano você só pode colocar no máximo 10 fotos. Você já tem ' + fotos + ' fotos.');
      if( coordenadas_atuais && (coordenadas_atuais == coordenadas_padrao) ) erros.push('Confirme seu endereço no mapa!');
      if( area_atuacao == false ) erros.push('Escolha pelo menos uma área de atuação.');
      if( cidade == false ) erros.push('Escolha pelo menos uma cidade de atuação.');
      

      /* Reutilizando para mostrar os erros no DOM
      */
      exibirErros = function( listaErros ){
        ul = $('#msgs_erros ul');
        ul.html('');
        for( key in listaErros ){
          ul.append('<li>' + listaErros[key] + '</li>');
        }
        
        msgs_erros = $('#msgs_erros');
        $('html, body').animate({ scrollTop: msgs_erros.offset().top - 20 }, 800);      
        msgs_erros.fadeOut('fast').fadeIn('fast');

        $('#publish').val(form.data('publish_original')).removeAttr('disabled').removeClass('button-primary-disabled').css('cursor','pointer');
      };


      /* Se já existir erros antes do Ajax, mostre-os
      */
      if( erros.length ) exibirErros( erros );
      else{

        jQuery.ajax({
          url: ajaxurl,
          type: 'POST',
          data: {
            'action': 'kadum_validacao_ajax',
            'post_id': id,
            'plano': plano,
            'titulo': titulo,
            'telefone': telefone,
            'descricao': descricao,
            'endereco': endereco,
            'coordenadas_atuais': coordenadas_atuais,
            'area_atuacao': area_atuacao,
            'cidade': cidade,
          },
          dataType: 'html',

          success: function( jsonErros ) {
            
            var json = JSON.parse(jsonErros);

            if( json.erros && json.erros.length > 0 ){
              exibirErros(json.erros);
            }
            
            else{
              $('#msgs_erros').fadeOut('fast');
              $("form#post").data('formulario_valido', true).submit();
            }

          },

          error: function(xhr, errorType, exception){
            erros = ['Houve um problema ao validar seu formulário. Tente novamente.'];
            exibirErros(erros);
          }

        });
      
      }

      return false;

    });
    
  });

})(jQuery);