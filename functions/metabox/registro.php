<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * ----------------------------------------------------------------------------
 * Aqui é feito o registro dos campos personalizados(metabox) para os Anúncios
 * 
 * É criado apenas um MetaBox, que contem todos campos personalizados necessários:
 *   - Seleção do plano de ofertas;
 *   - Título; (personalizado, não o do Wordpress)
 *   - Telefone;
 *   - Descrição;
 *   - Foto;
 *   - Endereço;
 *   - Mapa no Google Mapas.
 * 
 * Aqui também é feita a validação dos dados para a criação do anúncio.
**/


/**
 * Para a criação dos campos personalizados
 * foi utilizada a classe Wp Alchemy. Detalhes:
 * 
 * @author    Dimas Begunoff
 * @copyright Copyright (c) 2011, Dimas Begunoff, http://farinspace.com/
 * @license   http://en.wikipedia.org/wiki/MIT_License The MIT License
 * @package   WPAlchemy
 * @version   0.2.1
 * @link      http://github.com/farinspace/wpalchemy/
 * @link      http://www.farinspace.com/wpalchemy-metabox/
**/
require_once(metabox.ds.'wp_alchemy.class.php');


/**
 * Para redimensionar as imagens dos anúncios
 * foi utilizada a classe Canvas. Detalhes:
 * 
 * @author     Davi Ferreira <contato@daviferreira.com>
 * @version    1.0 $ 2010-10-17 19:11:51 $
**/
require_once(metabox.ds.'canvas.class.php');


/**
 *
 * Registro de todos os campos personalizados(metabox) a partir daqui.
 *
 * ATENÇÃO: Foi adicionado um paramêtro a mais chamado $postbox no WPAlchemy
 *          e serve para ativar ou desativar a classe css .postbox do Wordpress.
 *          Esta classe faz o campo personalizado ficar dentro de um box quando
 *          se está setado $context = 'advanced' ou 'normal'
 *          O valor padrão para a variavel $postbox é false.
 *          Detalhes na documentação do tema localizada no root.
**/


/*** GANCHOS ***/

# Adiciona um callback Ajax para adicionar miniaturas nos anúncios
add_action('wp_ajax_kadum_add_attachments', 'kadum_add_attachments');

# Adiciona um callback Ajax para remover miniaturas nos anúncios
add_action('wp_ajax_kadum_remove_attachments', 'kadum_remove_attachments');

# Consultar status do anúncio
add_action('wp_ajax_kadum_consultar_status', 'kadum_consultar_status');

# Adiciona um callback Ajax para validar o formulário do anúncio
add_action('wp_ajax_kadum_validacao_ajax', 'kadum_validacao_ajax');

# Desabilita o auto_save nos anúncios.
add_filter( 'script_loader_src', 'hacky_autosave_disabler', 10, 2 );


# Obtém as opções salvas do Kadum
$opcoes = get_option('opcoes-kadum');

# Campos personalizados
$campos_mb = new WPAlchemy_MetaBox(array(
    'id' => 'kadum_campos',
    'title' => 'Crie o seu anúncio',
    'template' => metabox.ds.'controlador.php',
    'types' => array('anuncios'),
    'context' => 'advanced',
    'priority' => 'high',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_kd_',

    'hide_title' => TRUE,
    'hide_editor' => TRUE,
    'lock' => WPALCHEMY_LOCK_BEFORE_POST_TITLE,
    'view' => WPALCHEMY_VIEW_ALWAYS_OPENED,
    'hide_screen_option' => TRUE,
    'postbox' => false, # Reescrita do valor padrão, pode-se apagar isso
    'autosave' => false,

    'head_filter' => 'kadum_add_css_js_metabox',
    'save_filter' => 'kadum_validacao_antes_salvar',
    'save_action' => 'kadum_validacao_depois_salvar',

));


# Inclusão do CSS e JS para personalização dos MetaBox
# Essa função não precisa ser adicionada a algum hook do Wordpress
# pois ela já está setada nas opções da classe, que se encarrega disso.
function kadum_add_css_js_metabox(){

    if( is_admin() ) {

        global $opcoes;

        # Adiciona Folha de estilo e Javascript ao template
        wp_enqueue_style('kadum_metabox_estilo', ab_metabox . '/css/estilo.css');
        wp_enqueue_style('thickbox');

        wp_enqueue_script('kadum_metabox_javascript_google_maps', 'http://maps.googleapis.com/maps/api/js?key='.$opcoes['config_gmaps_token'].'&sensor=true');
        wp_enqueue_script('kadum_metabox_javascript_lib', ab_metabox . '/js/javascript.lib.js');
        wp_enqueue_script('kadum_metabox_javascript', ab_metabox . '/js/metabox.js');
        wp_enqueue_script('ajaxupload', ab_metabox . '/js/ajaxupload.js');
        wp_enqueue_script('json2');
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        

    }
};



# Função executada antes de salvar o MetaBox
# Essa função não precisa ser adicionada a algum hook do Wordpress
# pois ela já está setada nas opções da classe, que se encarrega disso.
function kadum_validacao_antes_salvar($meta, $post_id){

    /**
     * Trata os dados passados pelo usuário
    **/
    return kadum_validacao_formulario(false, $meta, $post_id);
}



/**
 * Essa função
 *
**/
function kadum_validacao_depois_salvar($meta, $post_id){
    
    global $post;
    global $wpdb;

    # Gera o novo slug
    $titulo = esc_attr($meta['titulo']);
    $slug = sanitize_title($meta['titulo']);

    # Se o slug gerado é diferente do atual
    if( $post->post_name != $slug ){

        # Verifica se é possível salvar com este slug
        $verified_key = 1;
        $novo_slug = $slug;
        while( $verified_key != 0 ){

            # Tenta obter algum anúncio com o slug gerado
            $post_slug = get_page_by_path($novo_slug, OBJECT, 'anuncios');

            # Se existe, e não é ele mesmo, é preciso gerar um novo slug
            if( isset($post_slug) && ($post_slug->ID != $post_id) ){
                $novo_slug = $slug . '-' . $verified_key;
                $verified_key++;
            }

            else $verified_key = 0;
        }

        # Salva o novo slug!
        $wpdb->update(
            'wp_posts',
            array(
              'post_name' => $novo_slug,
              'post_title' => $titulo,
              //'post_status' => 'publish'
            ),
            array( 'ID' => $post_id ),
            array( '%s', '%s', '%s' ),
            array( '%d' )
        );
    
    }/*else{

        # Salva o novo slug!
        
        $wpdb->update(
            'wp_posts',
            array('post_status' => 'publish'),
            array( 'ID' => $post_id ),
            array( '%s' ),
            array( '%d' )
        );

    }*/
}

# Função executada antes de salvar o MetaBox
# Essa função não precisa ser adicionada a algum hook do Wordpress
# pois ela já está setada nas opções da classe, que se encarrega disso.
function kadum_validacao_formulario($return_json = false, $meta = null, $post_id = null){

    /**
     * Trata os dados passados pelo usuário
    **/

    global $post;
    global $opcoes;
    global $wpdb;

    $meta = ( isset($meta) && is_array($meta) ) ? $meta : $_POST;

    $post_id = ( isset($post_id) ) ? $post_id : $_REQUEST['post_id'];

    $plano = ( isset($meta['plano']) && in_array($meta['plano'], array('1','2','3')) ) ? $meta['plano'] : '1';

    /**
     * Os erros são mostrados todos de uma vez, através do wp_die()
     * Caso queira mostrá-los um por um, substitua o '$erros[] =' por wp_die()
    **/
    
    # Este array armazena todos os erros da validação
    $erros = array();

    # Verifica se tem um plano escolhido
    if(! ( isset($meta['plano']) && in_array($meta['plano'], array('1','2','3')) ) )
        $erros[] = ('Parece que não há um plano escolhido, isso é estranho.<br/>Crie/edite seu anúncio novamente.');

    # Valida o título do anúncio
    if(! isset($meta['titulo']) ) $erros[] = 'Ora... escreva um título para o seu anúncio!';
    else{
        if(strlen($meta['titulo']) > 40) $erros[] = 'Nossa, o título do seu anúncio está um pouco grande! Use no máximo 40 caracteres.';
        elseif(strlen($meta['titulo']) < 3) $erros[] = 'Nossa, que título pequeno! Pode falar mais!';
        else $meta['titulo'] = esc_attr($meta['titulo']);
    }

    # Valida o telefone do anúncio
    if(! isset($meta['telefone']) ) $erros[] = 'Escreva um telefone para o seu anúncio!';
    else{
        # Validação do telefone
        if(! preg_match('/^(\((1[2-9]|[2-9][0-9])\)\s?|(\(11\)\s?9))\d{4}[.-]?\d{4}$/', $meta['telefone']) )
            $erros[] = 'Seu telefone está em um padrão errado. Coloque o DDD entre parênteses.<br/>Por exemplo: (00) 0000-0000';
    }

    # Valida a descrição do anúncio
    if(! isset($meta['descricao']) ) $erros[] = 'Escreva uma descrição para o seu anúncio! Seus clientes querem saber mais!';
    else{
        $max = $opcoes['plano'.$plano.'_descricao_maximo'];

        # Validação da descrição
        $meta['descricao'] = strip_tags($meta['descricao']);
        if(strlen($meta['descricao']) > $max) $erros[] = 'Nossa, a descrição do seu anúncio está um pouco grande! Use no máximo '.$max.' caracteres.';
        if(strlen($meta['descricao']) < 60) $erros[] = 'Nossa, a descrição do seu anúncio está muito pequena. Use no mínimo 60 caracteres.';
    }

    # Valida, e faz o upload das fotos
    $fotos = get_posts(array(
        'post_type' => 'attachment',
        'numberposts' => null,
        'post_status' => null,
        'post_parent' => $post_id
    ));
    if(! $fotos ) $erros[] = 'Escolha pelo menos uma imagem para o seu anúncio!';
    elseif( count($fotos) > $opcoes['plano'.$plano.'_nfotos'] ){
        
        # Bom, de alguma forma obscura, tem foto demais!
        # Vamos pedir para o usuário remover estas fotos.
        $remova = count($fotos) - $opcoes['plano'.$plano.'_nfotos'];

        # Se o plano é o gratuito ou intermediário, ele pode remover, ou migrar de plano.
        if( $plano == 1 || $plano == 2 ){
            $erros[] = 'No plano ' . $opcoes['plano'.$plano.'_nome'] . ', você só pode adicionar ' . $opcoes['plano'.$plano.'_nfotos'] . ' fotos. Remova ' . $remova . ' fotos, ou, migre de plano.';
        }else if( $plano == 3 ){
            $erros[] = 'No plano ' . $opcoes['plano'.$plano.'_nome'] . ', você só pode adicionar ' . $opcoes['plano'.$plano.'_nfotos'] . ' fotos. Remova ' . $remova . ' fotos.';
        }
    }

    # Valida o endereço do anúncio
    if( isset($meta['endereco']) ){
        if( $meta['endereco'] == '' ) $erros[] = 'Escreva um endereço para o seu anúncio!';
        else{
            # Validação do endereço
            $meta['endereco'] = strip_tags($meta['endereco']);
            if(strlen($meta['endereco']) > 120) $erros[] = 'Nossa, o seu endereço está um pouco grande! Use no máximo 120 caracteres.';
            if(strlen($meta['endereco']) < 10) $erros[] = 'Nossa, o seu endereço está muito pequeno. Use no mínimo 10 caracteres.';
        }
    }

    # Valida a marcação no MAPA
    if( isset($meta['coordenadas_atuais']) ){
        if( $meta['coordenadas_atuais'] == $opcoes['config_coordenadas'] ){
            $erros[] = 'Marque um local no mapa para o seu anúncio!';
        }else{        
            # Validação da descrição
            $coordenadas = explode(',', $meta['coordenadas_atuais']);
            if( !is_numeric($coordenadas[0]) && !is_numeric($coordenadas[1]) ) $erros[] = 'Tem algo errado com a marcação no seu mapa. Tente novamente.';
        }
    }

    # Valida se o usuário marcou área de atuação ou cidade.
    if( $return_json == true ){
        if( isset($meta['area_atuacao']) && $meta['area_atuacao'] == 'false' ) $erros[] = 'Escolha uma área de atuação para o seu anúncio.';
        if( isset($meta['cidade']) && $meta['cidade'] == 'false' ) $erros[] = 'Escolha uma cidade para o seu anúncio.';
    }

    if( $return_json == true ) return $erros;

    else{

        # Exibe os erros
        if( count($erros) > 0 ){
            $msg = '<ul>';
            foreach ($erros as $value) $msg .= '<li>' . $value . '</li>';
            $msg .= '</ul>';

            $msg .= '<a class="button button-primary" href="' . admin_url('post.php') . '?post=' . $post_id . '&action=edit&kd_restore=true">&laquo; Voltar</a>';

            wp_die($msg);
        }
    }

    return $meta;
}


/**
 * Esta página administra a remoção de imagens e retorna as miniaturas atualizadas.
**/
function kadum_validacao_ajax() {


    /**
     * Esta função não faz mais nada que chamar outra, que fará a validação.
    **/
    $erros = kadum_validacao_formulario(true);
    $json = array();

    # Exibe os erros
    if( count($erros) > 0 ) $json['erros'] = $erros;
    else $json['sucesso'] = true;

    header('HTTP/1.0 200 OK');
    echo utf8_encode(json_encode($json));

    die();
}


/**
 *
**/
function hacky_autosave_disabler( $src, $handle ) {
    if( 'autosave' != $handle )
        return $src;
    return '';
}


/**
 * Esta página administra a remoção de imagens e retorna as miniaturas atualizadas.
**/
function kadum_add_attachments() {

    if( isset($_REQUEST['_wpnonce']) && isset($_REQUEST['_post_id']) && isset($_FILES['foto_kadum']) ){

        add_image_size('kadum_thumbnail', 300, 225, TRUE);

        $filename = $_FILES['foto_kadum']['name'];
        $wp_filetype = wp_check_filetype( basename($filename), null );
        $wp_upload_dir = wp_upload_dir();
        
        # Trata o nome da imagem, esta função está no kadum.lib.php
        $filename = date('d_m_Y-H_i_s-') . esc_file_upload(utf8_decode($filename));
        $path = $wp_upload_dir['path'] . '/' . $filename;
        $url = $wp_upload_dir['url'] . '/' . $filename;
        
        move_uploaded_file( $_FILES['foto_kadum']['tmp_name'] , $path );

        # Redimensiona!
        $resize = new canvas( $path );
        $resize->redimensiona(300, 225, 'crop')->grava($path);

        # Cria os metadados
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
            'post_content' => '',
            'post_status' => 'inherit',
            'guid' => $url,
        );
        $attach_id = wp_insert_attachment( $attachment, $filename, $_REQUEST['_post_id'] );        

        # Isto é necessário para que a função wp_generate_attachment_metadata() funcione.
        require_once(ABSPATH . 'wp-admin/includes/image.php');

        $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
        wp_update_attachment_metadata( $attach_id, $attach_data );

        $nonce_unique = 'nonce_attachment_' . $attach_id;
        $nonce = wp_create_nonce($nonce_unique);

        header('Content-type: application/json');

        $html = '<a class="foto-miniatura">';
        $html .= '<meta name="kd_foto_nonce" value="' . $nonce . '">';
        $html .= '<meta name="kd_foto_id" value="' . $attach_id . '">';

        $html .= wp_get_attachment_image( $attach_id );
        $html .= '<div class="remove-box"><div title="Remover foto">X</div></div>';
        $html .= '</a>';

        echo json_encode(array($html));

    }else{
        header('HTTP/1.0 500 Internal Server Error');
        echo 'Ocorreu um erro interno no Kadum. Tente novamente em instantes.';
    }

    die();
}


/**
 * Esta função remove miniaturas do anúncio.
**/
function kadum_remove_attachments() {
    
    if( isset($_REQUEST['_wpnonce']) && isset($_REQUEST['_attachment_id']) ) {
        $nonce_unique = 'nonce_attachment_' . $_REQUEST['_attachment_id'];
        $nonce = $_REQUEST['_wpnonce'];
        $verified = wp_verify_nonce($nonce, $nonce_unique);

        if( $verified ){
            
            $delete = wp_delete_attachment( $_REQUEST['_attachment_id'], true );
            if( false === $delete ){

                header('HTTP/1.0 500 Internal Server Error');
                echo 'Ocorreu um problema de segurança no Kadum. Tente novamente em instantes.';
            
            }

        }else{

            header('HTTP/1.0 500 Internal Server Error');
            echo 'Ocorreu um erro interno no Kadum. Tente novamente em instantes.';

        }
    }else{
        header('HTTP/1.0 500 Internal Server Error');
        echo 'Ocorreu um erro interno no Kadum. Tente novamente em instantes.';        
    }

    die();
}


/**
 * Esta consulta o status de pagamento do anúncio
**/
function kadum_consultar_status() {
    
    $id = ( isset($_REQUEST['id']) ) ? $_REQUEST['id'] : null;
    
    if( isset($id) ){

        $status = get_post_meta($id, '_kds_status', TRUE);

        # Status de pagamento
        $nomes_status = array(
            0 => 'Aguardando pagamento',
            1 => 'Cancelado',
            2 => 'Pagamento confirmado',
        );

        if( isset($status) && ($status == '') ){
            header('HTTP/1.0 500 Internal Server Error');
            echo 'Ocorreu um erro interno no Kadum. Tente novamente em instantes.';
        
        }else echo $nomes_status[$status];


    }else{

        header('HTTP/1.0 500 Internal Server Error');
        echo 'Ocorreu um erro interno no Kadum. Tente novamente em instantes.';

    }

    die();

}