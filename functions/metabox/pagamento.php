<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * ----------------------------------------------------------------------------
 * Aqui é feito o registro dos campos personalizados(metabox) para os Anúncios
 * 
**/

require_once(metabox.ds.'pagseguro'.ds.'PagSeguroLibrary.php');

class NovoAnuncio {
  
  public static function main () {
    
    # Informações do usuário logado.
    global $current_user, $plano, $opcoes;

    # Nova requisição de pagamento
    $paymentRequest = new PagSeguroPaymentRequest();

    # Adiciona item: Plano do Kadum
    $produto = 'plano_' . $plano;
    $nome    = $opcoes['plano'.$plano.'_nome'];
    $qtd     = 1;
    $preco   = number_format($opcoes['plano'.$plano.'_preco'], 2, '.', '');

    $paymentRequest->addItem($produto, $nome, $qtd, $preco);

    # Adiciona o cliente
    $paymentRequest->setSender(
      $current_user->data->display_name,
      $current_user->data->user_email
    );

    # Adiciona o endereço de entrega (endereço do kadum)
    $paymentRequest->setShippingAddress(
        '01452002',
        'Av. Brig. Faria Lima',
        '1384',
        'apto. 114',
        'Jardim Paulistano',
        'São Paulo',
        'SP',
        'BRA'
    );

    # Configura a moeda para Real do Brasil
    $paymentRequest->setCurrency("BRL");

    # Informação de frete
    $paymentRequest->setShippingType(3);

    # Código único
    $paymentRequest->setReference("I9635"); 

    
    try {
      
      /*
      * #### Crendencials ##### 
      * Substitute the parameters below with your credentials (e-mail and token)
      * You can also get your credentails from a config file. See an example:
      * $credentials = PagSeguroConfig::getAccountCredentials();
      */      
      
      # Informando as credenciais
      $credentials = new PagSeguroAccountCredentials(
        $opcoes['config_pagseguro_email'],
        $opcoes['config_pagseguro_token']
      );

      $redirectURL = 'http://www.kadum.com.br/pagamentos';
    
      # Fazendo a requisição a API do PagSeguro pra obter a URL de pagamento
      $url_pagamento = $paymentRequest->register($credentials);  
      
    } catch (PagSeguroServiceException $e) {
      die($e->getMessage());
    }
    
  }
  
  public static function printPaymentUrl($url) {
    if ($url) {
      echo "<h2>Criando requisição de pagamento</h2>";
      echo "<p>URL do pagamento: <strong>$url</strong></p>";
      echo "<p><a title=\"URL do pagamento\" href=\"$url\">Ir para URL do pagamento.</a></p>";
    }
  }
  
}



/**
 *
**/

class SearchTransactionByCode {

  public static function main() {
    
    $transaction_code = '59A13D84-52DA-4AB8-B365-1E7D893052B0';
    
    try {
      
      /*
      * #### Crendencials ##### 
      * Substitute the parameters below with your credentials (e-mail and token)
      * You can also get your credentails from a config file. See an example:
      * $credentials = PagSeguroConfig::getAccountCredentials();
      */    
      $credentials = new PagSeguroAccountCredentials('admin@nanpos.com', '223B145FF8E943249755C4C2AB73D57E');
      
      $transaction = PagSeguroTransactionSearchService::searchByCode($credentials, $transaction_code);
      
      self::printTransaction($transaction);
      
    } catch (PagSeguroServiceException $e) {
      die($e->getStatus());
    }
    
  }
  
  
  public static function printTransaction(PagSeguroTransaction $transaction) {
    
    echo "<h2>Transaction search by code result";
    echo "<h3>Code: " .     $transaction->getCode() .'</h3>'; 
    echo "<h3>Status: " .     $transaction->getStatus()->getTypeFromValue() .'</h3>'; 
    echo "<h4>Reference: " .  $transaction->getReference() . "</h4>";
    
    if ($transaction->getSender()) {
      echo "<h4>Sender data:</h4>";
      echo  "Name: ".   $transaction->getSender()->getName()  .'<br>'; 
      echo  "Email: ".  $transaction->getSender()->getEmail() .'<br>'; 
      if ( $transaction->getSender()->getPhone() ) {
        echo  "Phone: ". $transaction->getSender()->getPhone()->getAreaCode() . " - " . $transaction->getSender()->getPhone()->getNumber();
      }
    }
    
    if ($transaction->getItems()) {
      echo "<h4>Items:</h4>";
      if (is_array($transaction->getItems())) {
        foreach($transaction->getItems() as $key => $item) {
          echo "Id: ".        $item->getId()        .'<br>'; // prints the item id, p.e. I39
          echo "Description: ".     $item->getDescription()   .'<br>'; // prints the item description, p.e. Notebook prata
          echo "Quantidade: ".    $item->getQuantity()    .'<br>'; // prints the item quantity, p.e. 1
          echo "Amount: ".      $item->getAmount()      .'<br>'; // prints the item unit value, p.e. 3050.68
          echo "<hr>";
        }
      }
    }
    
    if ($transaction->getShipping()) {
      echo "<h4>Shipping information:</h4>";
      if ($transaction->getShipping()->getAddress()) {
        echo "Postal code: ". $transaction->getShipping()->getAddress()->getPostalCode().'<br>';
        echo "Street: ".      $transaction->getShipping()->getAddress()->getStreet().'<br>';
        echo "Number: ".    $transaction->getShipping()->getAddress()->getNumber().'<br>';
        echo "Complement: ".  $transaction->getShipping()->getAddress()->getComplement().'<br>';
        echo "District: ".    $transaction->getShipping()->getAddress()->getDistrict().'<br>';
        echo "City: ".      $transaction->getShipping()->getAddress()->getCity().'<br>';
        echo "State: ".     $transaction->getShipping()->getAddress()->getState().'<br>';
        echo "Country: ".     $transaction->getShipping()->getAddress()->getCountry().'<br>';
      }
      echo "Shipping type: ".   $transaction->getShipping()->getType()->getTypeFromValue().'<br>';
      echo "Shipping cost: ". $transaction->getShipping()->getCost().'<br>';
    }
    
    
  }
  
}