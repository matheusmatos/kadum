<div id="kadum_metabox" data-id="<?php echo $post->ID; ?>">

	<!-- Kadum Seleção dos Planos -->
	<div id="kd_planos" class="kadum_meta">

		<div id="msgs_erros">
			<a class="fechar">X</a>
			<h4>Foram encontrados os seguintes erros:</h4>
			<ul></ul>
		</div>

		<!-- PLANO ATUAL -->
		<?php $mb->the_field('plano'); $mb->meta[$mb->name] = $plano; ?>
		<input type="hidden" name="<?php $mb->the_name('plano'); ?>" value="<?php $mb->the_value('plano'); ?>" />

		<div class="table-wrap">
			<div class="coluna width30">
				<div class="plano p1">
					<h2><?php echo $opcoes['plano1_nome']; ?></h2>
					<ul class="vantagens">
						<?php foreach ($opcoes['plano1_vantagens'] as $key => $value): ?>
						<li><?php echo $value; ?></li>
						<?php endforeach; ?>
					</ul>
					<div class="plano-preco">
						<h4><small>R$</small> <?php echo number_format($opcoes['plano1_preco'], 2, ',', '.'); ?></h4>
					</div>
					<div class="status-escolha">
						<?php if($plano==1): ?>
						<span class="msg-escolhido"><strong></strong> Plano Escolhido</span>
						<?php else: ?>
						<a href="<?php echo $url . '&plano=1'; ?>" class="botao-escolher button button-primary">escolher</a>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<div class="coluna width40">
				<div class="plano p2">
					<h2><?php echo $opcoes['plano2_nome']; ?></h2>
					<ul class="vantagens">
						<?php foreach ($opcoes['plano2_vantagens'] as $key => $value): ?>
						<li><?php echo $value; ?></li>
						<?php endforeach; ?>
					</ul>
					<div class="plano-preco">
						<h4><small>R$</small> <?php echo number_format($opcoes['plano2_preco'], 2, ',', '.'); ?></h4>
					</div>
					<div class="status-escolha">
						<?php if($plano==2): ?>
						<span class="msg-escolhido"><strong></strong> Plano Escolhido</span>
						<?php else: ?>
						<a href="<?php echo $url . '&plano=2'; ?>" class="botao-escolher button button-primary">escolher</a>
						<?php endif; ?>
					</div>
				</div>
			</div>

			<div class="coluna width30">
					<div class="plano p3">
						<h2><?php echo $opcoes['plano3_nome']; ?></h2>
						<ul class="vantagens">
							<?php foreach ($opcoes['plano3_vantagens'] as $key => $value): ?>
							<li><?php echo $value; ?></li>
							<?php endforeach; ?>
						</ul>
						<div class="plano-preco">
							<h4><small>R$</small> <?php echo number_format($opcoes['plano3_preco'], 2, ',', '.'); ?></h4>
						</div>
						<div class="status-escolha">
							<?php if($plano==3): ?>
							<span class="msg-escolhido"><strong></strong> Plano Escolhido</span>
							<?php else: ?>
							<a href="<?php echo $url . '&plano=3'; ?>" class="botao-escolher button button-primary">escolher</a>
							<?php endif; ?>
						</div>
					</div>
			</div>
		</div><!-- .table-wrap -->

	</div><!-- #kd_planos -->




	<!-- PagSeguro! -->

	<?php if( $opcoes['plano'.$plano.'_preco'] > 0 ): ?>
	<div id="kadum_pagamento" class="kadum_meta">

		<?php $mb->the_field('pagseguro'); ?>
		<input type="hidden" name="<?php $mb->the_name('pagseguro'); ?>" value="<?php $mb->the_value(); ?>"/>
		
		<div class="icone-pagamento">
			<img class="icone-pagamento" src="<?php echo ab_metabox; ?>/img/basket.png" />
		</div>

		<div class="desc-pagamento">

			<?php if( isset($PagSeguro_Error) ): ?>

				<h2><?php echo $PagSeguro_Error['titulo']; ?></h2>
				<p><?php echo $PagSeguro_Error['descricao']; ?></p>

			<?php elseif( $status == 2 ): ?>

				<h2>Pagamento efetuado</h2>
				<p>Seu pagamento já foi confirmado e seu anúncio já está liberado.</p>

			<?php else: ?>

			<h2>Efetue o pagamento <small>Status: <?php echo $nomes_status[$status]; ?></small><a id="consultar-status" data-id="<?php echo $post->ID; ?>">Atualizar status</a></h2>
			<p>Seu anúncio será liberado automaticamente assim que o pagamento for confirmado pelo banco.</p>

			<br class="separador"/>

			<p>Pague com:</p>
			<ul class="icones">
				<li title="Saldo PagSeguro" class="pagseguro"></li>
				<li title="Visa" class="visa"></li>
				<li title="MasterCard" class="mastercard"></li>
				<li title="Diners" class="diners"></li>
				<li title="American Express" class="americanexpress"></li>
				<li title="HiperCard" class="hipercard"></li>
				<li title="Aura" class="aura"></li>
				<li title="Elo" class="elo"></li>
				<li title="PlenoCard" class="plenocard"></li>
				<li title="PersonalCard" class="personalcard"></li>
				<li title="OiPaggo" class="oipaggo"></li>
				<li title="Banco Bradesco" class="bradesco"></li>
				<li title="Banco Itaú" class="itau"></li>
				<li title="Banco do Brasil" class="bb"></li>
				<li title="Banco Banrisul" class="banrisul"></li>
				<li title="Banco HSBC" class="hsbc"></li>
				<li title="Boleto Bancário" class="boleto"></li>
				<li title="PinCode PagSeguro" class="pincode"></li>
			</ul>

			<a id="realizar_pagamento" class="botao-pagamento button" href="<?php echo $url_pagamento; ?>" target="_blank">Clique aqui para realizar o pagamento</a>
			<!--<p id="realizando_pagamento"><i class="ajax-loading"></i> Aguarde um momento...</p>-->
			
			<?php endif; ?>

		</div>

	</div>
	<?php endif; ?>





	<!-- Título! -->

	<div id="kadum_titulo" class="kadum_meta">

		<?php $mb->the_field('titulo'); ?>
		<div class="label-desc">
			<h4>Título</h4>
			<span>Escreva um título atraente para o seu anúncio.</span>
		</div>
		<div class="label">
			<input id="string_titulo" type="text" maxlength="40" name="<?php $mb->the_name('titulo'); ?>" value="<?php $mb->the_value('titulo'); ?>"/>
			<span>40 caracteres restantes</span>
			
			<!-- SLUG -->
			<p id="seu-slug">www.kadum.com.br/anuncio/<span><?php echo $post->post_name; ?></span></p>
		</div>

	</div>




	<!-- Telefone! -->

	<div id="kadum_telefone" class="kadum_meta">
		
		<?php $mb->the_field('telefone'); ?>
		<div class="label-desc">
			<h4>Telefone</h4>
			<span>Escreva o seu telefone, para ser exibido em seu anúncio</span>
		</div>
		<div class="label">
			<input type="text" pattern="(\((1[2-9]|[2-9][0-9])\)\s?|(\(11\)\s?9))\d{4}[.-]?\d{4}" name="<?php $mb->the_name('telefone'); ?>" value="<?php $mb->the_value('telefone'); ?>"/>
			<span>Digite o DDD entre parênteses e em seguida o número.<br/>Por exemplo: (31) 0000.0000</span>
		</div>

	</div>




	<!-- Descrição! -->

	<div id="kadum_descricao" class="kadum_meta">
		
		<?php $mb->the_field('descricao'); ?>
		<div class="label-desc">
			<h4>Descrição</h4>
			<span>Escreva um título para o seu anúncio</span>
		</div>
		<div class="label">		
			<textarea maximo="<?php echo $opcoes['plano'.$plano.'_descricao_maximo']; ?>" name="<?php $mb->the_name('descricao'); ?>"><?php $mb->the_value('descricao'); ?></textarea>
			<span>Descreva seu estabelecimento acima</span>
		</div>
	</div>




	<!-- Fotos! -->

	<?php if($opcoes['plano'.$plano.'_nfotos']): ?>
	<div id="kadum_fotos" class="kadum_meta">
		
		<div class="label-desc">
			<h4>Fotos</h4>
			<span>Selecione imagens para o seu anúncio</span>
		</div>

    <div class="label">
  		<div class="msgs status">
  			<p>Removendo...</p>
  			<a class="button ok">Ok</a>
  		</div>

  		<div class="msgs confirm">
  			<p>Você tem certeza que deseja remover esta miniatura?</br>Esta ação não poderá ser desfeita</p>
  			<a id="confirmar" class="button">Sim</a>
  			<a id="cancelar" class="button">Cancelar</a>
  		</div>    		

      <?php $fotos = get_posts(array(
          'post_type' => 'attachment',
          'numberposts' => null,
          'post_status' => null,
          'post_parent' => $post->ID
      ));

      $permitido = ( isset($fotos) ) ? $opcoes['plano'.$plano.'_nfotos'] - count($fotos) : null;

      /**
       *  Se existir fotos, mostrar as que tem.
       *  Se não for o máximo, mostra o botão
       **/
      ?>

      <div id="fotos_galeria">
      	
      	<?php if( isset($fotos) && ( count($fotos) > 0 ) ): foreach($fotos as $key => $foto):

            $nonce_unique = 'nonce_attachment_' . $foto->ID;
            $nonce = wp_create_nonce($nonce_unique);
        	?>

	        <a class="foto-miniatura">
            <meta name="kd_foto_nonce" value="<?php echo $nonce; ?>">
            <meta name="kd_foto_id" value="<?php echo $foto->ID; ?>">

            <?php echo wp_get_attachment_image( $foto->ID ); ?>
            <div class="remove-box"><div title="Remover foto">X</div></div>
	        </a>

        <?php endforeach; endif; ?>
      </div>
        
      <div id="fotos_upload" class="<?php if($permitido <= 0) echo 'oculto'; ?>">

      	<?php
          $nonce_post = 'nonce_new_attachment_' . $post->ID;
          $nonce_upload = wp_create_nonce($nonce_post);
      	?>
        <meta name="kd_nfotos" value="<?php echo $opcoes['plano'.$plano.'_nfotos']; ?>">
        <meta name="post_id" value="<?php echo $post->ID; ?>">
        <meta name="nonce_upload" value="<?php echo $nonce_upload; ?>">

	      <p id="msgStatus"></p>
	      <p>No plano <strong><?php echo $opcoes['plano'.$plano.'_nome']; ?></strong>, você pode fazer o upload de até <abbr id="fotos_maximo"><?php echo $opcoes['plano'.$plano.'_nfotos']; ?></abbr> fotos.</p>
	      <p>Você ainda pode fazer o upload de <abbr id="fotos_permitido"><?php echo $permitido; ?></abbr> fotos.</p>

	      <button id="botao_upload_disabled" disabled="disabled" class="button button-hero button-disabled">Aguarde...</button>
	      <a id="botao_upload" class="button button-primary button-hero">Fazer upload</a>

      </div>

    </div>
	</div>
	<?php endif; ?>
	



	
	<!-- Endereço! -->

	<?php if( isset($opcoes['plano'.$plano.'_endereco']) ): ?>
	<div id="kadum_endereco" class="kadum_meta">
		
		<div class="label-desc">
			<h4>Endereço</h4>
			<span>Escreva um título para o seu anúncio</span>
		</div>
		<div class="label">
			<input type="text" maxlength="120" name="<?php $mb->the_name('endereco'); ?>" value="<?php $mb->the_value('endereco'); ?>"/>
			<span>Escreva seu endereço</span>
		</div>

	</div>
	<?php endif; ?>




	<!-- Mapa! -->

	<?php if( isset($opcoes['plano'.$plano.'_mapa']) ): ?>
	
	<!-- COORDENADAS GEOGRAFICAS -->
	<?php $mb->the_field('coordenadas'); $mb->meta[$mb->name] = $coordenadas; ?>
	<input id="coordenadas-atuais" type="hidden" name="<?php $mb->the_name('coordenadas'); ?>" value="<?php $mb->the_value('coordenadas'); ?>" />	
	<input id="coordenadas-padrao" type="hidden" value="<?php echo $opcoes['config_coordenadas']; ?>" />	

	<?php $JaDefiniu = ($mb->get_the_value('coordenadas') == $opcoes['config_coordenadas']) ? false : true; ?>
	
	<div id="kadum_mapa" class="kadum_meta">
		
		<div class="msgs status">
			<p>Removendo...</p>
			<a class="button ok">Ok</a>
		</div>

		<div class="msgs confirm">
			<p>Você tem certeza que deseja remover esta miniatura?</br>Esta ação não poderá ser desfeita</p>
			<a id="confirmar" class="button">Sim</a>
			<a id="cancelar" class="button">Cancelar</a>
		</div>

		<div id="info_mostrar_no_mapa" class="<?php echo ($JaDefiniu) ? 'oculto' : ''; ?>">
			<div class="esq">
				<h4>Após digitar seu endereço, atualize o mapa.</h4>
				<p>Clique no botão para tentarmos localizar seu endereço no mapa. Caso não consigamos, você precisará marcar o endereço manualmente.</p>
			</div>
			
			<a id="mostrar_mapa" class="button button-primary button-hero">Mostrar endereço no mapa</a>
		</div>

		<?php if($JaDefiniu): ?>
			<div id="modificar_local_mapa">
				<p class="align-right">Você já definiu seu local no mapa <a class="button">Modificar local no mapa</a></p>
			</div>
		<?php endif; ?>
		
		<div id="kadum_frame_mapa"></div>

	</div>
	<?php endif; ?>

</div>