<?php

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Aqui é feito o registro do tipo de post personalizado "Anuncios"
 * Foram atribuidos taxonomias a este tipo de post, consulte o arquivo de
 * registro das taxonomias, por padrão, em <functions>/taxonomias.php
 * 
 * Aqui é também criado dois status de posts: aguardando_pagamento e expirado.
**/


# Registro dos tipos personalizados
add_action('init', 'kadum_anuncios', 8);

# 
add_action('init', 'kadum_posts_status', 9);

function kadum_anuncios() {

    $args = array(
        'labels' => array(
            'name'               => __('Anúncios', 'kadumtheme'),
            'singular_name'      => __('Anúncio', 'kadumtheme'),
            'add_new'            => __('Criar anúncio', 'kadumtheme'),
            'add_new_item'       => __('Criar novo anúncio', 'kadumtheme'),
            'edit_item'          => __('Editar anúncio', 'kadumtheme'),
            'new_item'           => __('Novo anúncio', 'kadumtheme'),
            'view_item'          => __('Ver anúncio', 'kadumtheme'),
            'search_items'       => __('Procurar anúncios', 'kadumtheme'),
            'not_found'          => __('Nenhum anúncio encontrado', 'kadumtheme'),
            'not_found_in_trash' => __('Nenhum anúncio encontrado na lixeira', 'kadumtheme'),
            'parent_item_colon'  => '',
            'menu_name'          => 'Anúncios'
        ),
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,         
        'query_var' => true,
        'rewrite' => array(
        	'slug' => 'anuncio',
            'with_front' => false
        ),
        'capability_type' => 'post',
        'has_archive' => 'anuncio',
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => ab_metabox . '/img/anuncios_icon.png',
        'supports' => array('slug')
      );

    register_post_type( 'anuncios' , $args );

    # Deseja tags nos anuncios? Descomete a linha abaixo:
    # register_taxonomy_for_object_type('post_tag', 'anuncios');

    flush_rewrite_rules();

};



/**
 * 
**/
function kadum_posts_status(){

    register_post_status('aguardando_pagamento', array(
        'label'                     => _x('Aguardando pagamento', 'anuncios'),
        'public'                    => false,
        'exclude_from_search'       => true,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Aguardando pagamento <span class="count">(%s)</span>', 'Aguardando pagamento <span class="count">(%s)</span>' ),
    ));

    register_post_status('expirado', array(
        'label'                     => _x('Expirados', 'anuncios'),
        'public'                    => false,
        'exclude_from_search'       => true,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Expirado <span class="count">(%s)</span>', 'Expirados <span class="count">(%s)</span>' ),
    ));
}