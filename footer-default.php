  </section><!-- #conteudo -->

  <footer id="rodape">
    
    <div class="container rodape-top">
      <div class="row">
        
        <div class="span4">
          <?php dynamic_sidebar('rodape-1'); ?>
        </div>
        
        <div class="span4">
          <?php dynamic_sidebar('rodape-2'); ?>
        </div>
        
        <div class="span4">
          <?php dynamic_sidebar('rodape-3'); ?>
        </div>

      </div>
    </div>
    <div class="container rodape-bottom">
      <p class="pull-left">&copy; Copyright <?php echo date('Y'); ?> - Todos os direitos reservados</p>
      <p class="pull-right">Kadum.com.br</p>
    </div>

  </footer>

  <?php get_footer(); ?>