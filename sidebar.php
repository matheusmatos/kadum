    
    <div id="kadum-lateral" class="span3">

        <?php global $user_login; ?>

        <div class="row">
            <div class="span3" id="kd_autenticacao">
                <?php if( is_user_logged_in() ): ?>

                    <?php global $current_user; get_currentuserinfo(); ?>

                <div class="usuario-logado row-fluid">
                    <div class="span4">
                        <img src="<?php echo get_avatar_url(get_avatar( $current_user->ID, 72 )); ?>" class="block img-polaroid"/>
                    </div>
                    
                    <div class="span8">
                        <ul class="nav">
                            <li><a href="<?php echo esc_url( admin_url('profile.php') ); ?>">Editar meu perfil</a></li>
                            <li><a href="<?php echo get_author_posts_url($current_user->ID); ?>">Meus anúncios</a></li>
                            <li><a href="<?php echo esc_url( admin_url('post-new.php?post_type=anuncios') ); ?>">Criar anúncio</a></li>
                            <li><a href="<?php echo wp_logout_url( home_url('/') ); ?> ">Sair</a></li>
                        </ul>
                    </div>
                </div>

                <?php else: ?>

                <form class="kd_formulario" action="<?php bloginfo('url') ?>/wp-login.php" method="post">
                    <h3>Entre no Kadum</h3>
                    <input class="span12" type="text" name="log" value="<?php echo esc_html(stripslashes($user_login), 1) ?>" placeholder="Email">
                    <input type="password" class="span12" name="pwd" placeholder="Senha">
                    <label><input name="rememberme" type="checkbox" checked="checked" value="forever"> Lembre-me</label>
                    <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>

                    <button type="submit" class="btn btn-info btn-block">Entrar &raquo;</button>
                    
                    <div class="separador-texto"> <em>ou</em> </div>
                    
                    <a href="<?php echo home_url('/') . 'criar-conta'; ?>" class="btn btn-primary btn-success btn-block">Crie uma conta &raquo;</a>
                </form>

                <?php endif; ?>
            </div>
        </div>
        
    </div>