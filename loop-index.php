<?php
/**
 * The loop that displays posts.
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop.php or
 * loop-template.php, where 'template' is the loop context
 * requested by a template. For example, loop-index.php would
 * be used if it exists and we ask for the loop with:
 * <code>get_template_part( 'loop', 'index' );</code>
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */


global $campos_mb;
global $post;

$loop = new WP_Query( array(
	'post_type' => 'anuncios',
	'posts_per_page' => 15,
	'post_status'    => 'publish',
	'meta_key'			 => '_kd_plano',
	'orderby' 			 => 'meta_value_num',
	'meta_query'     => array(
		array(
			'key' => '_kds_status',
			'value' => 2
		),
		array(
			'key' => '_kds_expira',
			'value' => date('Y-m-d'),
			'type' => 'DATE',
			'compare' => '>='
		)
	)	
));

?>

				<ul class="thumbnails">
					<?php while( $loop->have_posts() ): $loop->the_post(); $meta = $campos_mb->the_meta();
		  		
					$fotos = get_posts(array(
					  'post_type' => 'attachment',
					  'numberposts' => null,
					  'post_status' => null,
					  'post_parent' => $loop->post->ID
					)); ?>

		  		<li class="span3">
						<div class="thumbnail">
							
						<?php if( isset($fotos) && ( count($fotos) > 0 ) ): /*foreach($fotos as $key => $foto):*/ ?>
							<?php echo wp_get_attachment_image( $fotos[0]->ID ); ?>
						<?php else: ?>
							<img alt="" src="<?php echo get_template_directory_uri(); ?>/img/miniatura-padrao.jpg">
						<?php endif; ?>

							<h3><a href="<?php the_permalink() ?>"><?php echo $meta['titulo']; ?></a></h3>
							<p><?php echo $meta['descricao']; ?></p>
							<p><a class="btn" href="<?php the_permalink() ?>"><i class="icon-search"></i></a></p>
						</div>
				  </li>
					<?php endwhile; ?>
				</ul>


			<?php if ( $loop->max_num_pages > 1 ) : ?>
				<div class="pagination pagination-large pagination-centered">
				  <ul>
				    <li class="active"><span>1</span></li>
				    <li><a href="#teste">2</a></li>
				    <li><a href="#teste">3</a></li>
				    <li><a href="#teste">4</a></li>
				    <li><a href="#teste">5</a></li>
				    <li><a href="#teste">6</a></li>
				    <li><a href="#teste">7</a></li>
				    <li><a href="#teste">8</a></li>
				    <li><a href="#teste">9</a></li>
				    <li><a href="#teste">10</a></li>

				    
				    <li><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></li>
				    <!--<li><a href="#proxima">&rsaquo;</a></li>-->
				    <li><a>&raquo;</a></li>
				  </ul>
				</div>
			<?php endif; ?>