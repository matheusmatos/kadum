<?php get_header('default'); ?>

	<?php the_breadcrumb(); ?>
	
	<!-- Start of Facebook Meta Tags by shailan (http://shailan.com) -->
	<meta property="og:title" content="Introducing dropdown menu effects" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://shailan.com/introducing-dropdown-menu-effects/" />
	<meta property="og:image" content="http://shailan.com/wp-content/uploads/wp-dropdown-menu-effects-thumbnail-200x200.jpg" />
	<meta property="og:site_name" content="shailan.com"/>
	<meta property="og:description" content="With the 1.6 update finally added jQuery effects to the dropdown menu widget. This version also brought many other features like, auto-recognizing dropdown.css in theme folder, specifiying an external theme URL, options for removing link..." />
	<!-- End of Facebook Meta Tags -->
	
	<div class="row">
		<div class="span12">

			<p>Tá aqui! - <code>single-anuncios.php</code></p>
			
			<h2>Anúncios</h2>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title() ?></a></h2>
				<div class="post-meta">Posted on <?php the_time('F j, Y') ?> in <?php the_category(', '); ?> | <?php comments_popup_link('Comments (0)', 'Comments (1)', 'Comments (%)'); ?></div>
				<div class="entry">
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div>
			</div>
			<?php endwhile; ?>
			<?php else : ?>
			<div class="post">
				<div class="post-meta-2">
					<p class="container">
						<span class="date">No Matches</span>
					</p>
				</div>
				<h2>Nenhum anúncio encontrado</h2>
				<div class="entry">
					<p>Não foi encontrado nenhum anúncio cadastrado. (esse loop será personalizado de acordo com prioridades definidas na página de configurações.</p>
				</div>
			</div>
			<?php endif; ?>
			<div id="paginate-index" class="fix">
				<p><span class="left"><?php previous_posts_link('&laquo; Previous') ?></span> <span class="right"><?php next_posts_link('Next &raquo;') ?></span></p>
			</div>
		</div>
	</div>
	<?php // include (TEMPLATEPATH . '/side-column-index.php'); ?>
</div>

<?php get_footer('default'); ?>