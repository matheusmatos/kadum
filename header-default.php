<?php get_header(); ?>

  <!-- Cabeçalho: Logotipo, menu de links, caixa de busca -->
  <header id="cabecalho">

    <!-- Cabeçalho para smartphones e tablets -->
    <!--<div class="hidden-desktop">
      <div class="navbar navbar-mobile navbar-fixed-top">
        <div class="navbar-inner">
          
          <a class="logo-navbar" href="#">Kadum</a>

          <?php wp_nav_menu(array(
            'theme_location'  => 'navegacao-superior',
            'container'       => false,
            'menu_class'      => 'nav',
            'menu_id'         => '',
            'fallback_cb'     => false,
            'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
            'depth'           => 1
          )); ?>           

          <form id="searchform" role="search" method="get" action="<?php echo home_url('/'); ?>" class="">
            <div class="input-append">
              <input id="s" type="text" class="span12" name="s" value="<?php the_search_query(); ?>" placeholder="Digite um termo..." >
            </div>
          </form>

        </div>
      </div>
    </div>-->

    <!-- Cabeçalho para desktops -->
    <div class="container visible-desktop">
      <div class="row">

        <!-- Logo -->
        <div class="span3">
          <a class="logo-default" href="<?php echo home_url('/'); ?>"></a>
        </div>

        <!-- Menu -->
        <div class="span6">
            <?php wp_nav_menu(array(
              'theme_location'  => 'navegacao-superior',
              'container'       => false,
              'menu_class'      => 'nav nav-pills',
              'menu_id'         => '',
              'fallback_cb'     => false,
              'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
              'depth'           => 1
            )); ?>
        </div>

        <!-- Caixa de busca -->
        <div class="span3">
          <form id="searchform" role="search" method="get" action="<?php echo home_url('/'); ?>" class="pull-right">
            <div class="input-append">
              <input id="s" type="text" name="s" value="<?php the_search_query(); ?>" placeholder="Digite um termo..." >
              <button class="btn" type="button"><i class="icon-search"></i></button>
            </div>
          </form>
        </div>          

      </div>
    </div>

  </header>



  <!-- Barra de Navegação -->
  <nav id="barra-navegacao" class="container container-navbar">
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container container-navbar">
         
          <!-- .btn-navbar é usado para alternar quando a barra for diminuida -->
          <div data-toggle="collapse" data-target=".nav-collapse">
            <a class="btn btn-navbar pull-right">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
          </div>
         
          <!-- Isto é modificado em telas maiores que 940px -->
          <div class="nav-collapse collapse">
            <ul class="nav">
              <?php wp_nav_menu(array(
                'theme_location' => 'navegacao-categorias',
                'container'      => false,
                'menu_class'     => '',
                'menu_id'        => '',
                'fallback_cb'    => false,
                'items_wrap'     => '%3$s',
                //'items_wrap'     => '<ul class="%2$s"><li class="nav-carrousel-prev">&laquo;</li><ul class="%2$s nav-carrousel-wrap">%3$s</ul><li class="nav-carrousel-next">&raquo;</li></ul>',
                'depth'          => 1
              )); ?>
              <li class="divider-vertical"></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </li>              
            </ul>
          </div>

        </div>
      </div>
    </div>
  </nav>

    

  <!-- Conteúdo -->
  <section id="conteudo" class="container container-conteudo">