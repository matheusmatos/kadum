<?php

/**
 * Template name: Pagamentos
 * 
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Aqui é feito o cadastro de usuários do Kadum
 * 
**/


# Esta página é como um template.
# Por isso, é adicionado o seu respectivo controlador.
require_once( metabox . ds . 'page_pagamentos_controller.php' );


get_header(); ?>

  <div class="container-mini">
    <div class="container-mini-inner" method="post">
      <a class="logo-default pull-center" href="<?php echo home_url('/'); ?>"></a>

      <?php if( isset($Pagamento_errors) ): ?>

      <p class="lead align-center">Ops.. pagamento não efetuado.</p>

      <p>Não foi possível verificar o status do Pagamento.</p>
      <p>Verifique no PagSeguro em que estado ele se encontra ou tente novamente mais tarde.</p>
      
      <a class="btn btn-primary" href="<?php echo esc_url( admin_url() ); ?>">Voltar ao painel &raquo;</a>

      <?php else: ?>

      <p class="lead align-center">Pagamento efetuado!</p>

      <p>Por favor, verifique a caixa de entrada de seu email. Você receberá uma mensagem com sua senha.</p>
      <p>(Se não encontrar a mensagem em sua caixa de entrada, cheque sua caixa de spam).</p>

      <p>No entanto, você já pode começar agora mesmo!</p>
      
      <a class="btn btn-primary" href="<?php echo esc_url( admin_url() ); ?>">Voltar ao painel &raquo;</a>
      
      <?php endif; ?>

    </div>
  </div> <!-- .container-mini -->

<?php get_footer(); ?>