<?php get_header(); ?>

  <div class="container-mini">
    <form class="container-mini-inner" method="post">
      <a class="logo-default pull-center" href="<?php echo home_url('/'); ?>"></a>

      <p class="lead align-center">Erro 404</p>

      <p>A página que você está procurando não foi encontrada.</p>
      <p>Talvez você tenha digitado algo errado ou o link é antigo.</p>
      
      <p class="align-center"><a class="btn btn-primary" href="<?php echo esc_url( home_url() ); ?>">&laquo; Voltar ao Kadum</a></p>
    </form>
  </div> <!-- .container-mini -->

<?php get_footer(); ?>