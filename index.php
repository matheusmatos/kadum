<?php get_header('default'); ?>
		
		<!-- Linha -->
		<div id="hero-index" class="hero-unit">
			<div class="page-header center">
				<h1>Seja bem vindo ao Kadum</h1>
				<p>No Kadum você encontra as melhores ofertas para a sua cidade.</p>
			
				<?php if( is_user_logged_in() ): ?>
					<p class="buttons-singin-or-login">
						<a class="btn btn-success btn-large" href="<?php echo esc_url( admin_url('post-new.php?post_type=anuncios') ); ?>">Olá <?php echo $user_identity; ?>, você já pode criar um anúncio &raquo;</a>
					</p>
				<?php else: ?>
					<p class="buttons-singin-or-login">
						<a class="btn btn-success btn-large" href="<?php echo home_url('/criar-conta'); ?>">Criar conta</a>
						<span>ou</span>
						<a class="btn btn-info btn-large" href="<?php echo home_url('/wp-login.php'); ?>">Fazer login</a>
					</p>
			  <?php endif; ?>
			</div>
			<div class="separate"></div>
		</div>

		<?php //get_template_part('loop','index'); ?>

    <div class="row">
      
      <?php
      	$taxonomies = get_terms('area_atuacao', array(
		        'hide_empty' => false,
		        'parent' => 0,
		        'orderby' => 'count',
		        'order' => 'DESC',
		      )
		    );
      ?>
      <div class="span3">
        <ul class="nav nav-list">
        	<h3>Categorias</h3>
        	<?php if( count($taxonomies) > 0 ) foreach($taxonomies as $taxonomia): ?>
          <li><a href="<?php echo get_term_link($taxonomia); ?>"><i class="icon-chevron-right"></i> <?php echo $taxonomia->name; ?></a></li>
        	<?php endforeach; ?>
        </ul>
      </div>

      <div class="span9">
      	<?php get_template_part('loop','index'); ?>
			</div><!-- .span9 -->

		</div><!-- .row -->

<?php get_footer('default'); ?>