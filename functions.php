<?php ob_start();

/**
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Aqui são definidas constantes que serão usadas em diversas
 * partes do tema, e é feita a inclusão dos arquivos que controlam
 * cada funcionalidade do tema do Kadum.
**/


/**
 * Constantes com nomes e localização dos diretórios.
 * 
 * Caso queira renomear algum diretório, lembre-se
 * de renomear nestas primeiras definições também!
**/

# Root do tema
define('root', get_stylesheet_directory());

# Separador de diretório
define('ds', DIRECTORY_SEPARATOR);

# Diretório com os arquivos estáticos do tema
define('assets', root . ds . 'assets');

# Diretório com todas as funcionalidades
define('functions', root . ds . 'functions');

# Opções do Kadum
define('opcoes', functions . ds . 'opcoes');

# Metabox do Kadum
define('metabox', functions . ds . 'metabox');

# Painel personalizado
define('painel', functions . ds . 'painel');

# Usuários personalizados (anunciantes)
define('usuarios', functions . ds . 'usuarios');



/**
 * Caminhos absolutos, para uso em URL.
**/

# URL absoluta para o root do tema
define('kadum_tema_url', get_stylesheet_directory_uri());

# URL absoluta para o diretório de funções
define('assets_uri', kadum_tema_url . '/assets');

# URL absoluta para o diretório de funções
define('ab_functions', kadum_tema_url . '/functions');

# URL absoluta para o diretório das metaboxes
define('ab_opcoes', ab_functions . '/opcoes');

# URL absoluta para o diretório das metaboxes
define('ab_metabox', ab_functions . '/metabox');

# URL absoluta para as funções de personalização do painel.
define('ab_painel', ab_functions . '/painel');

# URL absoluta para as funções de personalização do usuarios.
define('ab_usuarios', ab_functions . '/usuarios');



/**
 * Inclusão das funcionalidades
**/

# Biblioteca de funções para uso no Kadum
require_once functions . ds . 'kadum.lib.php';

# Ações rápidas na instalação do tema
require_once functions . ds . 'setup-theme.php';

# Este arquivo compila os arquivos less
require_once functions . ds . 'less.php';

# Adição do menu opções
require_once opcoes . ds . 'controlador.php';

# Registro dos tipos de posts personalizados
require_once functions . ds . 'tipos_personalizados.php';

# Registro das taxonomias
require_once functions . ds . 'taxonomias.php';

# Funcionamento das MetaBox (campos personalizados)
require_once metabox . ds . 'registro.php';

# Criação de grupo de usuários e atribuição de capacidade administrativa.
require_once usuarios . ds . 'registro.php';

# Personalização do painel
require_once painel . ds . 'controlador.php';

// - testes de integração com o pagseguro (e integrar meeesmo)
// - galeria de fotos com upload próprio, e não com a galeria de mídia do wp
// - api do google maps, ao digitar endereço, mostrar no mapa
// - CronJobs para expirar o anúncio (pegar os anúncios com validade de hoje e expirá-lo)
// - separar anúncios expirados e publicados via javascript..

require_once functions . ds . 'cronjobs.php';

# Widgets
require_once functions . ds . 'widgets.php';

# Menus
require_once functions . ds . 'menus.php';

# Folha de estilo com regras reutilizáveis apenas para o painel
add_action( 'init', 'kadum_add_css_reset' );
function kadum_add_css_reset(){
  if(is_admin()) wp_enqueue_style('kadum-reset', ab_functions . '/globais.css');
}