<?php

/**
 * Template name: Criar Conta
 * 
 * Tema desenvolvido exclusivamente ao Kadum.
 * Detalhes no arquivo docs.pdf no root do tema.
 * 
 * @author      Kadum.com.br
 * @copyright   Copyright (c) 2012, Kadum, http://www.kadum.com.br
 * @version     1.0
 * @link        http://www.kadum.com.br/
 * -------------------------------------------------------------------------
 * 
 * Aqui é feito o cadastro de usuários do Kadum
 * 
**/

# Esta página é como um template.
# Por isso, é adicionado o seu respectivo controlador.
require_once( usuarios . ds . 'controlador.php' );

get_header(); ?>

	<div class="container-mini">
		<form class="container-mini-inner form-cadastro" method="post">
	    <a class="logo-default pull-center" href="<?php echo home_url('/'); ?>"></a>

			<?php

			/**
			 *  Essa página está dividida em três situações:
			 *
			 *	1 - Se houve tentativa de registrar um novo usuário, exibir caso de sucesso ou fracasso.
			 *	2 - Se o usuário está logado, não precisa criar outra conta.
			 *  3 - Se não há usuário logado, mostrar formulário de registro.
			 *  
			 *  A variável que controla essas situações é a $cadastro->nova_conta
			**/



			/**
			 * 1 - Se houve tentativa de registrar um novo usuário, exibir caso de sucesso ou fracasso.
			 * ------------------------------------------------------------------------------------------------------
			 * 
			 * Caso $cadastro->nova_conta já exista, ela traz informações da tentativa de efetuação do cadastro.
			 * Se $cadastro->nova_conta é um integer (id do usuário), a conta foi criada com sucesso.
			 * Se $cadastro->nova_conta é um WP_Error, a conta não foi criada com sucesso.
			**/
		
	
			# Vamos lá! Se existe $nova_conta...
			if(! $cadastro->nova_conta == false ):
			
				# E se ela é um integer...
				if(is_int($cadastro->nova_conta)): ?>

					<p class="lead align-center">Conta criada com sucesso!</p>

					<p>Por favor, verifique a caixa de entrada de seu email. Você receberá uma mensagem com sua senha.</p>
					<p>(Se não encontrar a mensagem em sua caixa de entrada, cheque sua caixa de spam).</p>
					
					<?php if(empty($cadastro->erros['error_login'])): ?>
					<p>No entanto, você já pode começar agora mesmo!</p>
					<a class="btn btn-large btn-primary" href="<?php echo esc_url( admin_url('post-new.php?post_type=anuncios') ); ?>">Criar um anúncio &raquo;</a>
					<?php endif;


				
				# Mas se não é um integer, será um WP_Error...
				else:
					
					if( isset($cadastro->erros['api_erro']) ): ?>
						<p class="lead align-center"><?php echo $cadastro->erros['api_erro']['titulo']; ?></p>
					
						<?php echo $cadastro->erros['api_erro']['conteudo']; ?>

					<?php elseif( count($cadastro->erros) ): ?>
						<p class="lead align-center">Ops! Encontramos alguns erros:</p>
						<ul>
						<?php foreach($cadastro->erros as $erro): ?>
							<li><?php echo $erro; ?></li>
						<?php endforeach; ?>
						</ul>
						
						<a class="btn btn-primary" href="<?php the_permalink(); ?>">Ok, voltar &raquo;</a>
						<a class="btn" href="<?php echo home_url('/'); ?>">Cancelar</a>
					<?php else: ?>

						<p class="lead align-center">Erro! :(</p>
						<p>Ocorreu um erro interno, tente novamente em breve.</p>

						<a class="btn btn-primary" href="<?php the_permalink(); ?>">Ok, voltar &raquo;</a>
						<a class="btn" href="<?php echo home_url('/'); ?>">Cancelar</a>
					<?php endif; ?>

				<?php endif;


			/**
			 * 2 - Usuário logado, não precisa criar outra conta.
			 * -----------------------------------------------------------------------------------
			**/
			elseif( is_user_logged_in() ): ?>

				<p>Você já está logado como <a href="<?php echo get_author_posts_url( $current_user->ID ); ?>"><?php echo $user_identity; ?></a>. Não é preciso se registrar novamente.</p>

				<p>O que você deseja fazer?</p>
				<ul>
					<li><a href="<?php echo esc_url( home_url('/') ); ?>">Ir para a Página Inicial</a></li>
					<li><a href="<?php echo esc_url( admin_url('edit.php?post_type=anuncios') ); ?>">Ver meus anúncios</a></li>
					<li><a href="<?php echo esc_url( admin_url('profile.php') ); ?>">Alterar meus dados</a></li>
				</ul>

		    <a class="btn btn-large btn-danger" href="<?php echo wp_logout_url( get_permalink() ); ?>"><i class="icon-off icon-white"></i> Sair desta conta</a>

			<?php
			else:

			/**
			 * 3 - Se não há usuário logado, mostrar formulário de registro.
			 * ------------------------------------------------------------------------------------------------------
			 * 
			 * 
			 * 
			 * 
			**/
			?>
	    
	    <p>Para você começar a anunciar no Kadum, é muito fácil. Basta se cadastrar, e estará pronto para começar.</p>

	    <div class="separador-texto"><span>Através das redes sociais:</span></div>

	    <nav class="icones-sociais-cadastro">
	    	<a class="facebook" href="<?php the_permalink(); ?>?facebook=✓"></a>
	    	<a class="google" href="<?php the_permalink(); ?>?google=✓"></a>
	    	<a class="twitter" href="<?php the_permalink(); ?>?twitter=✓"></a>
	    </nav>

	    <div class="separador-texto"><span>Ou, preencha abaixo:</span></div>

	    <div class="form-label">
	    	<span class="form-label-title">Qual é o seu nome?</span>
	    	<input class="input-block-level" type="text" name="nome" placeholder="Por exemplo: João José">
	    </div>

	    <div class="form-label">
	    	<span class="form-label-title">Escolha um nome de usuário.</span>
	    	<input class="input-block-level" type="text" name="usuario" placeholder="Por exemplo: joaojose">
	    </div>

	    <div class="form-label">
	    	<span class="form-label-title">Digite seu email, por onde receberá sua senha.</span>
	    	<input class="input-block-level" type="text" name="email" placeholder="Por exemplo: joaojose@gmail.com">
	    </div>

	    <input type="hidden" name="email_modo">
	    <button class="btn btn-large btn-primary" type="submit">Cadastrar</button>
	    <a class="btn btn-large" href="<?php echo home_url(); ?>">Cancelar</a>

			<?php endif; ?>

	  </form>
	</div> <!-- .container-mini -->

<?php get_footer(); ?>